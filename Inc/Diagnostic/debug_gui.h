//
// Created by mad on 24.03.18.
//

#ifndef VRIKSER_DEBUG_GUI_H
#define VRIKSER_DEBUG_GUI_H

#include "stm32f1xx_hal.h"
#include "main.h"

#define POLYNOMIAL1_A_PARAMETER ((double)(-0.0013))
#define POLYNOMIAL1_B_PARAMETER ((double)(0.13))
#define POLYNOMIAL1_C_PARAMETER ((double)(-4.8))
#define POLYNOMIAL1_D_PARAMETER ((double)(81))

#define POLYNOMIAL2_A_PARAMETER ((double)(-0.00177))
#define POLYNOMIAL2_B_PARAMETER ((double)(0.165))
#define POLYNOMIAL2_C_PARAMETER ((double)(-5.8))
#define POLYNOMIAL2_D_PARAMETER ((double)(89.4))

#define POLYNOMIAL3_A_PARAMETER ((double)(-0.0045))
#define POLYNOMIAL3_B_PARAMETER ((double)(0.4))
#define POLYNOMIAL3_C_PARAMETER ((double)(-13))
#define POLYNOMIAL3_D_PARAMETER ((double)(169.4))

#define POLYNOMIAL4_A_PARAMETER ((double)(-0.00094))
#define POLYNOMIAL4_B_PARAMETER ((double)(0.1))
#define POLYNOMIAL4_C_PARAMETER ((double)(-4.2))
#define POLYNOMIAL4_D_PARAMETER ((double)(77.77))
void Debug_GUI();
#endif //VRIKSER_DEBUG_GUI_H
