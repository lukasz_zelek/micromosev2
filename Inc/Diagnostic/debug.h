//
// Created by mad on 24.03.18.
//

#ifndef VRIKSER_DEBUG_H
#define VRIKSER_DEBUG_H

#include "stm32f1xx_hal.h"
#include "main.h"

void DEBUG_MainScreen(void);

//HARDWARE TEST
void DEBUG_Leds(void);
void DEBUG_BattVoltage(void);
void DEBUG_Butons(void);
void DEBUG_DistSensors(void);
void DEBUG_IMU(void);
void DEBUG_Motors(void);
void DEBUG_Memory(void);
void DEBUG_I2C(void);

//SOFTWARE TEST
void DEBUG_Soft_WallDetection(void);
void DEBUG_Soft_Motors_PID(void);
void DEBUG_Soft_Motors_Rotate(void);
void DEBUG_Soft_KalmanPosition(void);
void DEBUG_Soft_KalmanAngle(void);
void DEBUG_Soft_Mapping(void);
void DEBUG_Soft_IMUProcessing(void);
void DEBUG_Soft_MemInfo(void);
void DEBUG_Soft_DataExtractor(void);

#endif //VRIKSER_DEBUG_H
