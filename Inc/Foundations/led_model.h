//
// Created by Łukasz Zelek on 18.10.2019.
//

#ifndef VRIKSER_LED_MODEL_H
#define VRIKSER_LED_MODEL_H
int LS(uint32_t x);
int LF(uint32_t x);
int LD(uint32_t x);
int RS(uint32_t x);
int RF(uint32_t x);
int RD(uint32_t x);

#endif //VRIKSER_LED_MODEL_H
