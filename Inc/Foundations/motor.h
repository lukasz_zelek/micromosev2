//
// Created by mad on 17.12.18.
//

#ifndef VRIKSER_MOTOR_H
#define VRIKSER_MOTOR_H

#include "stm32f1xx_hal.h"
#include "main.h"

void Motor_Left_TurnOn(MOTOR_Direction dir);
void Motor_Left_TurnOff(void);
void Motor_Right_TurnOn(MOTOR_Direction dir);
void Motor_Right_TurnOff(void);
void Motor_Left_PWM(uint16_t duty);
void Motor_Right_PWM(uint16_t duty);

#endif //VRIKSER_MOTOR_H
