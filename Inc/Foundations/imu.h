//
// Created by mad on 17.12.18.
//

#ifndef VRIKSER_IMU_H
#define VRIKSER_IMU_H

#include "stm32f1xx_hal.h"
#include "main.h"
#include "Foundations/mpu6050.h"

void IMU_MPU6050_Init(void);
void IMU_MPU6050_ReadRaw(uint16_t* raw_acc, int16_t* raw_gyro);

// PRZEKONWETOWAĆ BIBLIOTEKĘ DO NASZYCH POTRZEB!
//void IMU_MPU6050_I2C_WriteBits();
//void IMU_MPU6050_I2C_RegWrite();
//void IMU_MPU6050_I2C_RegRead();

#endif //VRIKSER_IMU_H
