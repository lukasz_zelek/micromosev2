//
// Created by Mad Scientist on 06.04.2019.
//

#ifndef VRIKSER_TRIG_SERIES_H
#define VRIKSER_TRIG_SERIES_H

// SIN(X) expanded into 4th order polynominal series
double Math_Trig_Series_sin(double x);

// COS(X) expanded into 4th order polynominal series
double Math_Trig_Series_cos(double x);

// TAN(X) expanded into 4th order polynominal series
double Math_Trig_Series_tan(double x);

// ARCSIN(X) expanded into 4th order polynominal series
double Math_Trig_Series_arcsin(double x);

// ARCTAN(X) expanded into 4th order polynominal series
double Math_Trig_Series_arctan(double x);

#endif //VRIKSER_TRIG_SERIES_H
