//
// Created by mad on 17.12.18.
//

#ifndef VRIKSER_DIST_SENSORS_H
#define VRIKSER_DIST_SENSORS_H

#include "stm32f1xx_hal.h"
#include "main.h"



void        DistSensorSetSensor(uint16_t ADC_DISTx);
uint16_t    DistSensorMeassure(uint16_t ADC_DISTx);
double      DistSensorCalculateMilimeters(uint32_t DistSensorWynik, ADC_DIST_SENSOR sensor);


void        DistSensorTurnOff(uint16_t ADC_DIST_x);
void        DistSensorTurnOffAll(void);
void        DistSensorTurnOn(uint16_t ADC_DIST_x);
void        DistSensorTurnOnAll(void);

void        DistSensorTimerStart(void);
void        DistSensorTimerStop(void);
void        DistSensorTimerCallback(void);

#endif //VRIKSER_DIST_SENSORS_H
