//
// Created by mad on 17.12.18.
//

#ifndef VRIKSER_BASIC_IO_H
#define VRIKSER_BASIC_IO_H

#include "stm32f1xx_hal.h"
#include "main.h"

void LED_Turn_OnAll(void);
void LED_Turn_On(uint16_t LEDx_Pin);
void LED_Turn_OffAll(void);
void LED_Turn_Off(uint16_t LEDx_Pin);
void LED_Toggle(uint16_t LEDx_Pin);

GPIO_PinState   BTN_IsPressed(uint16_t BTNx_Pin);
float Battery_Voltage_Measure(void);
void  UART_Send_String(char* String);

#endif //VRIKSER_BASIC_IO_H
