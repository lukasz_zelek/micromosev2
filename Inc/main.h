/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */
#include "constants.h"
/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define DIST1_ADC_Pin GPIO_PIN_0
#define DIST1_ADC_GPIO_Port GPIOC
#define DIST2_ADC_Pin GPIO_PIN_1
#define DIST2_ADC_GPIO_Port GPIOC
#define DIST3_ADC_Pin GPIO_PIN_2
#define DIST3_ADC_GPIO_Port GPIOC
#define DIST4_ADC_Pin GPIO_PIN_3
#define DIST4_ADC_GPIO_Port GPIOC
#define V_DIV_Pin GPIO_PIN_0
#define V_DIV_GPIO_Port GPIOA
#define PWMA_Pin GPIO_PIN_1
#define PWMA_GPIO_Port GPIOA
#define PWMB_Pin GPIO_PIN_2
#define PWMB_GPIO_Port GPIOA
#define EN_RIGHT1_Pin GPIO_PIN_6
#define EN_RIGHT1_GPIO_Port GPIOA
#define EN_RIGHT2_Pin GPIO_PIN_7
#define EN_RIGHT2_GPIO_Port GPIOA
#define DIST5_ADC_Pin GPIO_PIN_4
#define DIST5_ADC_GPIO_Port GPIOC
#define DIST6_ADC_Pin GPIO_PIN_5
#define DIST6_ADC_GPIO_Port GPIOC
#define BTN1_Pin GPIO_PIN_0
#define BTN1_GPIO_Port GPIOB
#define BTN1_EXTI_IRQn EXTI0_IRQn
#define BTN2_Pin GPIO_PIN_1
#define BTN2_GPIO_Port GPIOB
#define BTN3_Pin GPIO_PIN_2
#define BTN3_GPIO_Port GPIOB
#define MOTOR_LEFT_BWRD_Pin GPIO_PIN_12
#define MOTOR_LEFT_BWRD_GPIO_Port GPIOB
#define MOTOR_LEFT_FWD_Pin GPIO_PIN_13
#define MOTOR_LEFT_FWD_GPIO_Port GPIOB
#define LED1_Pin GPIO_PIN_6
#define LED1_GPIO_Port GPIOC
#define LED2_Pin GPIO_PIN_7
#define LED2_GPIO_Port GPIOC
#define LED3_Pin GPIO_PIN_8
#define LED3_GPIO_Port GPIOC
#define LED4_Pin GPIO_PIN_9
#define LED4_GPIO_Port GPIOC
#define DIST1_EN_Pin GPIO_PIN_8
#define DIST1_EN_GPIO_Port GPIOA
#define DIST2_EN_Pin GPIO_PIN_9
#define DIST2_EN_GPIO_Port GPIOA
#define DIST3_EN_Pin GPIO_PIN_10
#define DIST3_EN_GPIO_Port GPIOA
#define DIST4_EN_Pin GPIO_PIN_11
#define DIST4_EN_GPIO_Port GPIOA
#define DIST5_EN_Pin GPIO_PIN_12
#define DIST5_EN_GPIO_Port GPIOA
#define DIST6_EN_Pin GPIO_PIN_15
#define DIST6_EN_GPIO_Port GPIOA
#define INT_GYRO_Pin GPIO_PIN_3
#define INT_GYRO_GPIO_Port GPIOB
#define EN_LEFT1_Pin GPIO_PIN_6
#define EN_LEFT1_GPIO_Port GPIOB
#define EN_LEFT2_Pin GPIO_PIN_7
#define EN_LEFT2_GPIO_Port GPIOB
#define MOTOR_RIGHT_BWRD_Pin GPIO_PIN_8
#define MOTOR_RIGHT_BWRD_GPIO_Port GPIOB
#define MOTOR_RIGHT_FWD_Pin GPIO_PIN_9
#define MOTOR_RIGHT_FWD_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */
//Ulatwienia dla Meassurmentow
#define	ADC_DIST_RIGHT_FORWARD	ADC_CHANNEL_10
#define	ADC_DIST_LEFT_FORWARD	ADC_CHANNEL_11
#define	ADC_DIST_LEFT_DIAGONAL	ADC_CHANNEL_12
#define	ADC_DIST_RIGHT_DIAGONAL	ADC_CHANNEL_13
#define	ADC_DIST_RIGHT_SIDE	    ADC_CHANNEL_14
#define	ADC_DIST_LEFT_SIDE		ADC_CHANNEL_15

/*changed, becouse of infrared interference
typedef enum {
    LEFT_SIDE = 0U,
    LEFT_DIAGONAL,
    LEFT_FORWARD,
    RIGHT_FORWARD,
    RIGHT_DIAGONAL,
    RIGHT_SIDE
} ADC_DIST_SENSOR;
*/
typedef enum {
    LEFT_SIDE = 0U,
    RIGHT_FORWARD,
    LEFT_DIAGONAL,
    RIGHT_DIAGONAL,
    LEFT_FORWARD,
    RIGHT_SIDE
} ADC_DIST_SENSOR;

//ADRESY I2C
#define I2C_ADDR_IMU_MPU6050    0xD0 //+1
#define I2C_ADDR_MEMORY         0xA0 //+1


typedef enum {
    MOTOR_FWD = 0U, MOTOR_BWRD
} MOTOR_Direction;


#define UART_RX_BUFFOR_SIZE 1
#define UART_TX_BUFFOR_SIZE 150
#define ENCODER_COUNTS_TURN_LEFT 4000
#define ENCODER_COUNTS_TURN_RIGHT

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
