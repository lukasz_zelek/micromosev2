/*
* matrix.hpp
*
*  Created on: 11 Mar 2018
*      Author: ilona
*/

#ifndef VRIKSER_MATRIX_H
#define VRIKSER_MATRIX_H

#include <stdio.h>
#include <stdlib.h>

#define VRISKERMATRIX_SIZE  2
#define VRISKERVECTOR_SIZE  2

typedef float   Scalar;
typedef Scalar** Matrix;
typedef Scalar*  Vector;
typedef Vector  RowVector;


Matrix allocateMatrix(void);
Vector allocateVector(void);
void setMatrix(Matrix mat, Scalar val00, Scalar val01, Scalar val10, Scalar val11);
void setVector(Vector vec, Scalar val0, Scalar val1);
void freeMatrix(Matrix* mat);
void freeVector(Vector* vec);

void matrixAdd(Matrix in1, Matrix in2, Matrix acc);
void vectorAdd(Vector in1, Vector in2, Vector acc);
void matrixSub(Matrix in1, Matrix in2, Matrix acc);
void vectorSub(Vector in1, Vector in2, Vector acc);

void matrixTranspose(Matrix mat, Matrix acc);

void matrixMultScalar(Matrix in1, Scalar in2, Matrix acc);
void vectorMultScalar(Vector in1, Scalar in2, Vector acc);
void matrixMultVector(Matrix in1, Vector in2, Vector acc);
void matrixMultMatrix(Matrix in1, Matrix in2, Matrix acc);
void rowVectorMultMarix(RowVector in1, Matrix in2, RowVector acc);
Scalar rowVectorMultVector(RowVector in1, Vector in2);
void vectorMultRowVector(Vector in1, RowVector in2, Matrix acc);
#endif //VRIKSER_MATRIX_H
