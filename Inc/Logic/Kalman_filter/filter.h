//
// Created by mad on 26.03.18.
//

#ifndef VRIKSER_FILTER_H
#define VRIKSER_FILTER_H

#include <math.h>
#include "matrix.h"
#include "main.h"
#include "stm32f1xx_hal.h"
#include "Foundations/mpu6050.h"
#include "Logic/Control/motors.h"
#include "Logic/Control/wall_detection.h"
#include "Foundations/dist_sensors.h"
#include "Foundations/basic_io.h"

#include <stdlib.h>

//int** allocateMatrix(int rows, int cols, int** matrix);
void dataExtractor(void);
void orientationEstimator(void);
void routeEstimator();

#endif //VRIKSER_FILTER_H
