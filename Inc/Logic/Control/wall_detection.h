//
// Created by mad on 09.01.19.
//

#ifndef VRIKSER_WALL_DETECTION_H
#define VRIKSER_WALL_DETECTION_H

#include "Foundations/dist_sensors.h"
#define INITIAL_VALUE_CALLIBRATION 900
uint16_t DistSensorSMAInit(uint16_t* tab, uint16_t ADC_DIST_x); // TODO USUNAC
uint16_t DistSensorSMAStep(uint16_t* tab, uint16_t* sum, uint16_t ADC_DIST_x); // TODO USUNAC
void WallCheckCallibration(void);
uint8_t WallCheck_Left(void);
uint8_t WallCheck_Right(void);
uint8_t WallCheck_Front(void);



#endif //VRIKSER_WALL_DETECTION_H
