//
// Created by mad on 09.01.19.
//

#ifndef VRIKSER_MOTORS_H
#define VRIKSER_MOTORS_H

#include "Foundations/motor.h"

void Motors_Init(void);
void Motors_Forward(void);
void Motors_Backward(void);
void Motors_Stop(void);
void Motors_PID(float error);
void Motor_Left_Move(uint16_t encoder_counts);
void Motor_Right_Move(uint16_t enocder_counts);
void Motors_Turn_Left(uint16_t duty);
void Motors_Turn_Left90Degrees(void);
void Motors_Turn_Right(uint16_t duty);
void Motors_Turn_Right90Degrees(void);
void Motors_Turn_Right_Degrees(int angle);
void Motors_Turn_Left_Degrees(int angle);
void Motors_Turn_Around(void);
void Motors_ForwardCertainTimes(int revolutionCount, int dutyPercentage);
void Motors_BackwardCertainTimes(int revolutionCount, int dutyPercentage);

void Motors_Forward_Milimeters(double x, int detectCounter);



#endif //VRIKSER_MOTORS_H
