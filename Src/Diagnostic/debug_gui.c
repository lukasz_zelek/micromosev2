//
// Improved version of Debug
//

#include <math.h>
#include "Diagnostic/debug.h"
#include "Foundations/basic_io.h"
#include "Logic/Control/motors.h"
#include "Logic/Control/wall_detection.h"
#include "Diagnostic/debug_gui.h"


extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc3;
extern DMA_HandleTypeDef hdma_adc1;
extern I2C_HandleTypeDef hi2c2;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern TIM_HandleTypeDef htim6;
extern TIM_HandleTypeDef htim7;

extern UART_HandleTypeDef huart4;

extern uint8_t   FLAG_WALL_FRONT;
extern uint8_t   FLAG_WALL_LEFT;
extern uint8_t   FLAG_WALL_RIGHT;

//Zmienne do transmisji UARTa
extern uint8_t  RX_data[UART_RX_BUFFOR_SIZE];
extern uint8_t  TX_data[UART_TX_BUFFOR_SIZE];
extern uint16_t TX_size;
extern uint8_t  FLAG_UART_RECIVED;
extern int16_t  IMU_rawdata[6];

extern uint32_t* DistSensorWynik;

void Debug_GUI() {
    float VBat;
    DistSensorTimerStart();
    //Ustawianie PWMs
    int PWM_L = 0;
    int PWM_R = 0;

    //Odpalenie silnikow
    Motors_Init();
    while (RX_data[0] != 'q') {
        VBat = Battery_Voltage_Measure();

        char *tmpSign = (VBat < 0) ? "-" : "";
        float tmpVal = (VBat < 0) ? -VBat : VBat;

        int tmpInt1 = ((int) tmpVal);
        float tmpFrac = tmpVal - tmpInt1;
        int tmpInt2 = ((int) trunc((double) tmpFrac * 10000));

        //kalkulacja odleglosci w mm BEGIN
        double x = (double)DistSensorWynik[LEFT_SIDE]/100.0;
        double y = (double)DistSensorWynik[LEFT_FORWARD]/100.0;
        double z = (double)DistSensorWynik[RIGHT_FORWARD]/100.0;
        double w = (double)DistSensorWynik[RIGHT_SIDE]/100.0;

        uint8_t ls = (uint8_t)(POLYNOMIAL1_A_PARAMETER*x*x*x + POLYNOMIAL1_B_PARAMETER*x*x + POLYNOMIAL1_C_PARAMETER*x + POLYNOMIAL1_D_PARAMETER);
        if(x < 4)
            ls = (uint8_t)(-47*x+250);
        uint8_t lf = (uint8_t)(POLYNOMIAL2_A_PARAMETER*y*y*y + POLYNOMIAL2_B_PARAMETER*y*y + POLYNOMIAL2_C_PARAMETER*y + POLYNOMIAL2_D_PARAMETER);
        if(y < 4)
            lf = (uint8_t)(-45*y+250);
        uint8_t rf = (uint8_t)(POLYNOMIAL3_A_PARAMETER*y*y*y + POLYNOMIAL3_B_PARAMETER*y*y + POLYNOMIAL3_C_PARAMETER*y + POLYNOMIAL3_D_PARAMETER);
        if(z < 9)
            rf = (uint8_t)(-19*z+250);
        uint8_t rs = (uint8_t)(POLYNOMIAL4_A_PARAMETER*y*y*y + POLYNOMIAL4_B_PARAMETER*y*y + POLYNOMIAL4_C_PARAMETER*y + POLYNOMIAL4_D_PARAMETER);
        if(w < 4)
            rs = (uint8_t)(-47*w+250);
        //kalkulacja odleglosci w mm END


        TX_size = ((uint16_t) sprintf((char *) TX_data, "%i %i %i %i %i %i %i %i %s%d.%04d\n\r",
                                      ls,lf,rf,rs,
                                      (int) htim4.Instance->CNT, (int) htim3.Instance->CNT,PWM_L,PWM_R, tmpSign, tmpInt1, tmpInt2));
        HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
        if (FLAG_UART_RECIVED) {
            FLAG_UART_RECIVED = 0;
            switch (RX_data[0]) {

                case 'a':
                    if (PWM_L < 1200) { PWM_L += 1; }
                    break;

                case 'b':
                    if (PWM_L < 1200) { PWM_L += 2; }
                    break;

                case 'c':
                    if (PWM_L < 1200) { PWM_L += 4; }
                    break;

                case 'd':
                    if (PWM_L < 1200) { PWM_L += 8; }
                    break;

                case 'e':
                    if (PWM_L < 1200) { PWM_L += 16; }
                    break;

                case 'f':
                    if (PWM_L < 1200) { PWM_L += 32; }
                    break;

                case 'g':
                    if (PWM_L < 1200) { PWM_L += 64; }
                    break;

                case 'h':
                    if (PWM_L < 1200) { PWM_L += 128; }
                    break;

                case 'i':
                    if (PWM_L < 1200) { PWM_L += 256; }
                    break;

                case 'j':
                    if (PWM_L < 1200) { PWM_L += 512; }
                    break;

                case 'k':
                    if (PWM_R < 1200) { PWM_R += 1; }
                    break;

                case 'l':
                    if (PWM_R < 1200) { PWM_R += 2; }
                    break;

                case 'm':
                    if (PWM_R < 1200) { PWM_R += 4; }
                    break;

                case 'n':
                    if (PWM_R < 1200) { PWM_R += 8; }
                    break;

                case 'o':
                    if (PWM_R < 1200) { PWM_R += 16; }
                    break;

                case 'p':
                    if (PWM_R < 1200) { PWM_R += 32; }
                    break;

                case 'r':
                    if (PWM_R < 1200) { PWM_R += 64; }
                    break;

                case 't':
                    if (PWM_R < 1200) { PWM_R += 128; }
                    break;

                case 'u':
                    if (PWM_R < 1200) { PWM_R += 256; }
                    break;

                case 'v':
                    if (PWM_R < 1200) { PWM_R += 512; }
                    break;

                case 'w':
                    if (PWM_L > -1200) { PWM_L -= 1; }
                    break;

                case 'x':
                    if (PWM_L > -1200) { PWM_L -= 2; }
                    break;

                case 'y':
                    if (PWM_L > -1200) { PWM_L -= 4; }
                    break;

                case 'z':
                    if (PWM_L > -1200) { PWM_L -= 8; }
                    break;

                case '<':
                    if (PWM_L > -1200) { PWM_L -= 16; }
                    break;

                case '>':
                    if (PWM_L > -1200) { PWM_L -= 32; }
                    break;

                case '(':
                    if (PWM_L > -1200) { PWM_L -= 64; }
                    break;

                case ')':
                    if (PWM_L > -1200) { PWM_L -= 128; }
                    break;

                case '[':
                    if (PWM_L > -1200) { PWM_L -= 256; }
                    break;

                case ']':
                    if (PWM_L > -1200) { PWM_L -= 512; }
                    break;

                case '1':
                    if (PWM_R > -1200) { PWM_R -= 1; }
                    break;

                case '2':
                    if (PWM_R > -1200) { PWM_R -= 2; }
                    break;

                case '3':
                    if (PWM_R > -1200) { PWM_R -= 4; }
                    break;

                case '4':
                    if (PWM_R > -1200) { PWM_R -= 8; }
                    break;

                case '5':
                    if (PWM_R > -1200) { PWM_R -= 16; }
                    break;

                case '6':
                    if (PWM_R > -1200) { PWM_R -= 32; }
                    break;

                case '7':
                    if (PWM_R > -1200) { PWM_R -= 64; }
                    break;

                case '8':
                    if (PWM_R > -1200) { PWM_R -= 128; }
                    break;

                case '9':
                    if (PWM_R > -1200) { PWM_R -= 256; }
                    break;

                case '.':
                    if (PWM_R > -1200) { PWM_R -= 512; }
                    break;
                case '~':
                    htim3.Instance->CNT = 32768;
                    htim4.Instance->CNT = 32768;
                    break;
                case '`':
                    htim3.Instance->CNT = 0;
                    htim4.Instance->CNT = 0;
                    break;
            }//SWITCH

            if(PWM_L < 0)
            {
                Motor_Left_TurnOn(MOTOR_BWRD);
                Motor_Left_PWM(-PWM_L);
            }
            else{Motor_Left_TurnOn(MOTOR_FWD);
                Motor_Left_PWM(PWM_L);}

            if(PWM_R < 0)
            {
                Motor_Right_TurnOn(MOTOR_BWRD);
                Motor_Right_PWM(-PWM_R);
            }
            else{Motor_Right_TurnOn(MOTOR_FWD);
                Motor_Right_PWM(PWM_R);}
        }
        HAL_Delay(3);
    }
    DistSensorTimerStop();
    FLAG_UART_RECIVED = 0;
}