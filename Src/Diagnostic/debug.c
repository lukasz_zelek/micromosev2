//
// Created by mad on 24.03.18.
//

#include <math.h>
#include "Diagnostic/debug.h"
#include "Foundations/imu.h"
#include "Foundations/basic_io.h"
#include "Foundations/eeprom.h"
#include "Logic/Control/motors.h"
#include "Logic/Control/wall_detection.h"
#include "Foundations/led_model.h"

extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc3;
extern DMA_HandleTypeDef hdma_adc1;
extern I2C_HandleTypeDef hi2c2;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern TIM_HandleTypeDef htim6;
extern TIM_HandleTypeDef htim7;

extern UART_HandleTypeDef huart4;

extern uint8_t   FLAG_WALL_FRONT;
extern uint8_t   FLAG_WALL_LEFT;
extern uint8_t   FLAG_WALL_RIGHT;

//Zmienne do transmisji UARTa
extern uint8_t  RX_data[UART_RX_BUFFOR_SIZE];
extern uint8_t  TX_data[UART_TX_BUFFOR_SIZE];
extern uint16_t TX_size;
extern uint8_t  FLAG_UART_RECIVED;
/*extern*/ int16_t  IMU_rawdata_acc[3];
/*extern */int16_t  IMU_rawdata_gyro[3];

extern uint32_t* DistSensorWynik, *AmbientSensorWynik;


//MAIN MENU
void DEBUG_MainScreen(void){

    UART_Send_String("# VRISKER MICROMOUSE - DEBUG MODE #\n\r");
    UART_Send_String("Select an option:\n\r");
    UART_Send_String("[HARDWARE]\n\r");
    UART_Send_String("1) LEDs \t\t\t5) IMU\n\r");
    UART_Send_String("2) V_BAT \t\t\t6) Motors\n\r");
    UART_Send_String("3) Buttons \t\t\t7) Memory\n\r");
    UART_Send_String("4) DistSensors \t\t\t8) I2C\n\r");
    UART_Send_String("[SOFTWARE]\n\r");
    UART_Send_String("a) Wall Detection \t\tb) Kalman - Position \n\r");
    HAL_Delay(20);
    UART_Send_String("c) Motors PID\t\t\td) Kalman - Angle\n\r");
    UART_Send_String("e) Mapping \t\t\tf) IMU + Encoders\n\r");
    UART_Send_String("g) System Memory Info \t\th) Kalman - Data\n\r");
    HAL_Delay(20);
    UART_Send_String("j) Motors Rotate \t\tx) TEMP\n\r");

}



//HARDWARE TEST
void DEBUG_Leds(void){


    UART_Send_String(">Selected: DEBUG_Leds\n\r");
    UART_Send_String("{1-4} test leds. Press 'q' to exit\n\r");
    LED_Turn_OffAll();

    while(RX_data[0] != 'q') {

        if(FLAG_UART_RECIVED) {
            FLAG_UART_RECIVED = 0;
            switch (RX_data[0]) {

                case '1':
                    TX_size = (uint16_t)sprintf((char*)TX_data, "LED 1: Toggled\n\r");
                    LED_Toggle(LED1_Pin);
                    break;

                case '2':
                    TX_size = (uint16_t)sprintf((char*)TX_data, "LED 2: Toggled\n\r");
                    LED_Toggle(LED2_Pin);
                    break;

                case '3':
                    TX_size = (uint16_t)sprintf((char*)TX_data, "LED 3: Toggled\n\r");
                    LED_Toggle(LED3_Pin);
                    break;

                case '4':
                    TX_size = (uint16_t)sprintf((char*)TX_data, "LED 4: Toggled\n\r");
                    LED_Toggle(LED4_Pin);
                    break;

                default: // Jezeli odebrano nieobslugiwany znak
                    TX_size = (uint16_t)sprintf((char*)TX_data, "{1-4} test leds. Press 'q' to exit\n\r");
                    break;

            }
            HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
        }
    }
}
void DEBUG_BattVoltage(void){

    UART_Send_String(">Selected: DEBUG_BattVoltage\n\r");
    float VBat;

    //UART_Send_String("Press 'q' to exit.\n\r");
    while(RX_data[0] != 'q') {
        VBat = Battery_Voltage_Measure();

        //Formatowanie odczytu z floata
        char *tmpSign = (VBat < 0) ? "-" : "";
        float tmpVal = (VBat < 0) ? -VBat : VBat;

        int tmpInt1 = ((int)tmpVal);                  // Get the integer (678).
        float tmpFrac = tmpVal - tmpInt1;      // Get fraction (0.0123).
        int tmpInt2 = ((int)trunc((double)tmpFrac * 10000));  // Turn into integer (123).

        TX_size = ((uint16_t)sprintf((char*)TX_data, "'q' to exit. V_BAT = %s%d.%04d V \n\r", tmpSign, tmpInt1, tmpInt2));
        HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
        HAL_Delay(100);
    }
    FLAG_UART_RECIVED = 0;
}


void DEBUG_Butons(void){

    UART_Send_String(">Selected: DEBUG_Buttons\n\r");
    UART_Send_String("Press 'q' to exit.\n\r");
    while(RX_data[0] != 'q') {

        if(BTN_IsPressed(BTN1_Pin)){
            UART_Send_String("BTN1 has been pressed\n\r");
        }
        else if(BTN_IsPressed(BTN2_Pin)){
            UART_Send_String("BTN2 has been pressed\n\r");
        }
        else if(BTN_IsPressed(BTN3_Pin)){
            UART_Send_String("BTN3 has been pressed\n\r");
        }
        if(FLAG_UART_RECIVED){
            UART_Send_String("Press 'q' to exit.\n\r");
            FLAG_UART_RECIVED = 0;
        }

        HAL_Delay(100);
    }
}
void DEBUG_DistSensors(void){

    UART_Send_String(">Selected: DEBUG_DistSensors\n\r");

//    int LF, LD, LS, RF, RD, RS;
    LED_Turn_OffAll();
    DistSensorTimerStart();
    while(RX_data[0] != 'q') {

//        LF = (int)DistSensorWynik[LEFT_FORWARD];
//        LS = (int)DistSensorWynik[LEFT_SIDE];
//        LD = (int)DistSensorWynik[LEFT_DIAGONAL];
//        RF = (int)DistSensorWynik[RIGHT_FORWARD];
//        RS = (int)DistSensorWynik[RIGHT_SIDE];
//        RD = (int)DistSensorWynik[RIGHT_DIAGONAL];
//        LF = DistSensorCalculateMilimeters(DistSensorWynik[LEFT_FORWARD],LEFT_FORWARD);
//        LD = DistSensorCalculateMilimeters(DistSensorWynik[LEFT_DIAGONAL],LEFT_DIAGONAL);
//        RS = DistSensorCalculateMilimeters(DistSensorWynik[LEFT_SIDE],LEFT_SIDE);
//        LF = DistSensorCalculateMilimeters(DistSensorWynik[RIGHT_FORWARD],RIGHT_FORWARD);
//        RS = DistSensorCalculateMilimeters(DistSensorWynik[RIGHT_SIDE],RIGHT_SIDE);
//        RD = DistSensorCalculateMilimeters(DistSensorWynik[RIGHT_DIAGONAL],RIGHT_DIAGONAL);

        //TX_size = ((uint16_t)sprintf((char*)TX_data, "L_F = %i ; L_S = %i ; L_D = %i || R_F = %i ; R_S = %i ; R_D = %i\n\r", (int)(pow(DIST_SENSOR_MODEL_PARAM_A_LEFT_FORWARD, (double)DistSensorWynik[LEFT_FORWARD])), (int)DistSensorWynik[LEFT_SIDE], (int)LD, (int)RF, (int)RS, (int)RD));
        //TX_size = ((uint16_t)sprintf((char*)TX_data, "L_F = %i ; L_S = %i ; L_D = %i || R_F = %i ; R_S = %i ; R_D = %i\n\r", (int)DistSensorWynik[LEFT_FORWARD],(int)DistSensorWynik[LEFT_SIDE],(int)DistSensorWynik[LEFT_DIAGONAL],(int)DistSensorWynik[RIGHT_FORWARD], (int)DistSensorWynik[RIGHT_SIDE], (int)DistSensorWynik[RIGHT_DIAGONAL] ));
        TX_size = ((uint16_t)sprintf((char*)TX_data, "L_F = %i ; L_S = %i ; L_D = %i || R_F = %i ; R_S = %i ; R_D = %i\n\r", LF(DistSensorWynik[LEFT_FORWARD]),LS(DistSensorWynik[LEFT_SIDE]),LD(DistSensorWynik[LEFT_DIAGONAL]),RF(DistSensorWynik[RIGHT_FORWARD]), RS(DistSensorWynik[RIGHT_SIDE]), RD(DistSensorWynik[RIGHT_DIAGONAL]) ));
        //TX_size = ((uint16_t)sprintf((char*)TX_data, "L_F = %i %i; L_S = %i %i; L_D = %i %i || R_F = %i %i; R_S = %i %i; R_D = %i %i\n\r", (int)DistSensorWynik[LEFT_FORWARD],(int)AmbientSensorWynik[LEFT_FORWARD],(int)DistSensorWynik[LEFT_SIDE],(int)AmbientSensorWynik[LEFT_SIDE],(int)DistSensorWynik[LEFT_DIAGONAL],(int)AmbientSensorWynik[LEFT_DIAGONAL],(int)DistSensorWynik[RIGHT_FORWARD],(int)AmbientSensorWynik[RIGHT_FORWARD], (int)DistSensorWynik[RIGHT_SIDE],(int)AmbientSensorWynik[RIGHT_SIDE], (int)DistSensorWynik[RIGHT_DIAGONAL], (int)AmbientSensorWynik[RIGHT_DIAGONAL]));
        HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
        HAL_Delay(10);
        //TX_size = ((uint16_t)sprintf((char*)TX_data, "L_F = %i ; L_S = %i ; L_D = %i || R_F = %i ; R_S = %i ; R_D = %i\n\r", (int)AmbientSensorWynik[LEFT_FORWARD],(int)AmbientSensorWynik[LEFT_SIDE],(int)AmbientSensorWynik[LEFT_DIAGONAL],(int)AmbientSensorWynik[RIGHT_FORWARD], (int)AmbientSensorWynik[RIGHT_SIDE], (int)AmbientSensorWynik[RIGHT_DIAGONAL] ));
        //HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
        //HAL_Delay(10);
    }
    DistSensorTimerStop();
}
void DEBUG_IMU(void){
    UART_Send_String(">Selected: DEBUG_IMU\n\r");

    //uint8_t deviceID = MPU6050_GetDeviceID();


    while(RX_data[0] != 'q') {

        IMU_MPU6050_ReadRaw(((uint16_t*)&IMU_rawdata_acc),((int16_t*)&IMU_rawdata_gyro));
        //HAL_I2C_Mem_Read(&hi2c2, MPU6050_DEFAULT_ADDRESS, MPU6050_RA_ACCEL_XOUT_H, 1, wartosc, 1, 100);

        //uint8_t tmp;
        //MPU6050_I2C_BufferRead(MPU6050_DEFAULT_ADDRESS, &wartosc, MPU6050_RA_ACCEL_XOUT_H, 1);

        //RZECZYWISTE DANE / INTERPRETACJA
        //Oś Z - w górę oś oś X
        //Oś X - do przodu oś Y
        //Oś Y - w prawo oś Z
        TX_size = ((uint16_t)sprintf((char*)TX_data, "A_X = %i \t A_Y = %i \t A_Z = %i \t||\t G_R = %d \t G_P = %d \t G_Y = %d\n\r", IMU_rawdata_acc[2], IMU_rawdata_acc[0], IMU_rawdata_acc[1], IMU_rawdata_gyro[2], IMU_rawdata_gyro[0], IMU_rawdata_gyro[1]));
        HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
        HAL_Delay(10);
    }

}

void DEBUG_Motors(void){
    UART_Send_String(">Selected: DEBUG_Motors\n\r");

    //Pomiar napiecia
    float VBat;

    //Ustawianie PWMs
    uint16_t PWM_L=0;
    uint16_t PWM_R=0;

    //Odpalenie silnikow
    Motors_Init();

    while(RX_data[0] != 'q') {

        //Zabezpieczenie przed padnieciem baterii
        VBat = Battery_Voltage_Measure();

        //Formatowanie odczytu z floata
        char *tmpSign = (VBat < 0) ? "-" : "";
        float tmpVal = (VBat < 0) ? -VBat : VBat;

        int tmpInt1 = (int)tmpVal;                  // Get the integer (678).
        float tmpFrac = tmpVal - tmpInt1;      // Get fraction (0.0123).
        int tmpInt2 = (int)trunc((double)tmpFrac * 10000);  // Turn into integer (123).

        TX_size = ((uint16_t)sprintf((char*)TX_data, "PWM: w/s L=%i, e/d R=%i ; ENC: L=%i  R=%i ; V_BAT = %s%d.%04d ; dt=5ms \n\r", PWM_L, PWM_R, (int)htim4.Instance->CNT, (int)htim3.Instance->CNT, tmpSign, tmpInt1, tmpInt2));
        HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);

        if(FLAG_UART_RECIVED) {
            FLAG_UART_RECIVED = 0;
            switch (RX_data[0]) {

                case 'w':
                    if (PWM_L < 1200) { PWM_L += 50; }
                    break;

                case 's':
                    if (PWM_L > 0) { PWM_L -= 50; };
                    break;

                case 'e':
                    if (PWM_R < 1200) { PWM_R += 50; };
                    break;

                case 'd':
                    if (PWM_R > 0) { PWM_R -= 50; };
                    break;

                default: // Jezeli odebrano nieobslugiwany znak
                    TX_size = ((uint16_t)sprintf((char*)TX_data, "Press 'q' to exit\n\r"));
                    HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
                    break;

            }//SWITCH

            Motor_Right_PWM(PWM_L);
            Motor_Left_PWM(PWM_R);
        }//IF

        HAL_Delay(5);
    }//WHILE


}
void DEBUG_Memory(void){
    UART_Send_String(">Selected: DEBUG_Memory\n\r");

    UART_Send_String(">WRITING \"213\" to 0x0068@EEPROM\n\r");
    uint8_t temp = 213;
    //HAL_I2C_Mem_Write(&hi2c2, EEPROM_ADDRESS, 0x0069, EEPROM_MEMADD_SIZE_16BIT, &temp, 1, 100);
    uint32_t Tickstart = HAL_GetTick();
    EEPROM_Write(0x0068,&temp);
    uint32_t Tickend = HAL_GetTick() - Tickstart;
    TX_size = ((uint16_t)sprintf((char*)TX_data, ">WRITTEN IN %i ticks \n\r", (int)Tickend));
    HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
    HAL_Delay(10);

    UART_Send_String(">READING FROM 0x0068@EEPROM\n\r");
    uint8_t temp2 = 0;
    Tickstart = HAL_GetTick();
    EEPROM_Read(0x0068,&temp2);
    Tickend = HAL_GetTick() - Tickstart;
    //HAL_I2C_Mem_Read(&hi2c2,  EEPROM_ADDRESS, 0x0069, EEPROM_MEMADD_SIZE_16BIT, &temp2, 1, 100);

    TX_size = ((uint16_t)sprintf((char*)TX_data, ">0x0068@EEPROM = %i , readed in %i ticks\n\r", temp2, (int)Tickend));
    HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
    HAL_Delay(10);

    UART_Send_String(">READING FROM 0x0069@EEPROM\n\r");
    Tickstart = HAL_GetTick();
    EEPROM_Read(0x0069,&temp2);
    Tickend = HAL_GetTick() - Tickstart;
    TX_size = ((uint16_t)sprintf((char*)TX_data, ">0x0069@EEPROM = %i , readed in %i ticks \n\r", temp2, (int)Tickend));
    HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
    HAL_Delay(10);

    //TESTOWANIE PRZESYLANIA BLOKOW
    uint8_t data[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    uint8_t buffer[10];

    UART_Send_String(">WRITING DATA TABLE to 0x0070@EEPROM\n\r");
    EEPROM_Write_Bytes(0x0070, ((uint8_t*)&data), 10);
    UART_Send_String(">DATA HAS BEEN WRITTEN. VERIFYING:\n\r");
    int i=0;
    for(i=0; i<11; i++){
        //BARDZO BARDZOOO DZIWNY BŁAD!
        //i jest wywoływane od początku jako 1. WTF.
        EEPROM_Read(((uint16_t)(0x0070 + i-1)),&temp2);
        TX_size = ((uint16_t)sprintf((char*)TX_data, "DATA_TABLE[%i]=%i;  \n\r", i-1, temp2));
        HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
        HAL_Delay(20);
    }
    HAL_Delay(10);

    UART_Send_String(">READING DATA TABLE from 0x0070@EEPROM\n\r");
    EEPROM_Read_Bytes(0x0070, ((uint8_t*)&buffer), 10);
    UART_Send_String(">DATA HAS BEEN READ\n\r");
    HAL_Delay(10);
    for(i=0; i<10; i++){
        TX_size = ((uint16_t)sprintf((char*)TX_data, "buffer[%i]=%i;  \n\r", i, buffer[i]));
        HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
        HAL_Delay(10);
    }
    HAL_Delay(10);

}
void DEBUG_I2C(void){
    UART_Send_String(">Selected: DEBUG_I2C\n\r");

    //HAL_StatusTypeDef HAL_I2C_Mem_Write(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size, uint32_t Timeout)
//    I2C_HandleTypeDef *hi2C - wskaźnik na strukturę konfiguracyjną I2C.
//    uint16_t DevAddress - adres urządzenia, do którego mają zostać wysłane dane.
//    uint16_t MemAddSize - rozmiar rejestru pamięci (w bajtach).
//    uint8_t *pData - wskaźnik na pamięć, która zawiera dane do przesłania.
//    uint16_t Size - liczba bajtów danych do przesłania.
//    uint32_t Timeout - czas, który I2C może przeznaczyć na oczekiwanie na poprawne zakończenie komunikacji (w milisekundach).

    //HAL_I2C_Mem_Write(&hi2c1, LSM303_ACC_ADDRESS, LSM303_ACC_CTRL_REG1_A, 1, &Settings, 1, 100);
    uint8_t Data = 0; // Zmienna do bezposredniego odczytu z akcelerometru

    UART_Send_String("'q' to exit. Press 's' to start scan I2C_ADRs.\n\r");

    while(RX_data[0] != 'q') {

        if(FLAG_UART_RECIVED) {
            FLAG_UART_RECIVED = 0;
            switch (RX_data[0]) {

                case 's':
                    UART_Send_String("i2c@vrisker /debug/$ SCAN STARTED \n\r");
                    int i;
                    for(i=0;i<255; i++){
                        if(HAL_I2C_Mem_Read(&hi2c2, (uint16_t)i, 0x00, 1, &Data, 1, 100) == HAL_OK ){
                            TX_size = ((uint16_t)sprintf((char*)TX_data, "FOUND SLAVE @ R:0x%X W:0x%X\n\r", i, i+1));
                            i++;
                            HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
                            HAL_Delay(1);
                        }
                    }
                    UART_Send_String("i2c@vrisker /debug/$ SCAN ENDED \n\r");
                    UART_Send_String("'q' to exit. Press 's' to start scan I2C_ADRs.\n\r");
                    break;

                default: // Jezeli odebrano nieobslugiwany znak
                    UART_Send_String("'q' to exit. Press 's' to start scan I2C_ADRs.\n\r");
                    break;

            }//SWITCH
        }//IF
    }//WHILE

}//DEBUG_I2C

//SOFTWARE TEST
void DEBUG_Soft_WallDetection(void){
    UART_Send_String(">Selected: DEBUG_SOFT_WallDetection\n\r");

//    TX_size = ((uint16_t)sprintf((char*)TX_data, "Wall detection threshold: %i\n\r", DIST_WALL_DISTANCE_FRONTL));
//    HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
    HAL_Delay(10);

    TX_size = ((uint16_t)sprintf((char*)TX_data, "FRONT WALL DETECTED: %i \n\r", WallCheck_Front()));
    HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
    HAL_Delay(10);

    TX_size = ((uint16_t)sprintf((char*)TX_data, "RIGHT WALL DETECTED: %i \n\r", WallCheck_Right()));
    HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
    HAL_Delay(10);

    TX_size = ((uint16_t)sprintf((char*)TX_data, "LEFT  WALL DETECTED: %i \n\r", WallCheck_Left()));
    HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
    HAL_Delay(10);

}
void DEBUG_Soft_Motors_PID(void){
    UART_Send_String(">Selected: DEBUG_SOFT_MotorsPID\n\r");
    UART_Send_String("(Actually it's more than just a PID)\n\r");
    UART_Send_String("Press 'q' to exit.\n\r");
	
	//Uchyb regulacji
	int err = 0;

//	//Czujnii odbicia boczne
//	uint16_t leftSensor[DIST_SMA_SAMPLES], rightSensor[DIST_SMA_SAMPLES];
//    uint16_t leftSMAsum, rightSMAsum, rightSMA, leftSMA;
//    uint16_t rightZero, leftZero, avgZero;
//
//	//Czujniki odbicia diagonalne
//	//Regula: Jesli krecimy w danym kierunku, to diagonalny czujnik po tamtej stronie ma wieksza wartosc od drugiego
//	uint16_t diagLeftSensor[DIST_SMA_SAMPLES], diagRightSensor[DIST_SMA_SAMPLES];
//	uint16_t diagLeftSMAsum, diagRightSMAsum, diagRightSMA, diagLeftSMA;
//	//uint16_t diagLeftZero, diagRightZero;//, diagAvgZero;

	//Enkodery
    uint32_t encoderRight, encoderLeft;

//	//Inicjalizacja tablic pomiarami
//    leftSMAsum		= DistSensorSMAInit((uint16_t*)&leftSensor,  ADC_DIST_LEFT_SIDE);
//    rightSMAsum		= DistSensorSMAInit((uint16_t*)&rightSensor, ADC_DIST_RIGHT_SIDE);
//	diagLeftSMAsum	= DistSensorSMAInit((uint16_t*)&diagLeftSensor, ADC_DIST_LEFT_DIAGONAL);
//	diagRightSMAsum = DistSensorSMAInit((uint16_t*)&diagRightSensor, ADC_DIST_RIGHT_DIAGONAL);
//    rightSMA	    = DistSensorSMAStep((uint16_t*)&rightSensor, &rightSMAsum, ADC_DIST_RIGHT_SIDE);
//    leftSMA		    = DistSensorSMAStep((uint16_t*)&leftSensor, &leftSMAsum, ADC_DIST_LEFT_SIDE);
//    //diagLeftSMA     = DistSensorSMAStep((uint16_t*)&diagLeftSensor, &diagLeftSMAsum, ADC_DIST_LEFT_DIAGONAL);
//    //diagRightSMA    = DistSensorSMAStep((uint16_t*)&diagRightSensor, &diagRightSMAsum, ADC_DIST_RIGHT_DIAGONAL);
//
//    //Kalibraca (pozycja startowa)
//    leftZero		= leftSMA;//DistSensorSMAStep(&leftSensor, &leftSMAsum, ADC_DIST_LEFT_SIDE);
//    rightZero		= rightSMA;//DistSensorSMAStep(&rightSensor, &rightSMAsum, ADC_DIST_RIGHT_SIDE);
//    //diagLeftZero	= DistSensorSMAStep((uint16_t*)&diagLeftSensor,  &diagLeftSMAsum, ADC_DIST_LEFT_DIAGONAL);
//    //diagRightZero	= DistSensorSMAStep((uint16_t*)&diagRightSensor, &diagRightSMAsum, ADC_DIST_RIGHT_DIAGONAL);
//    avgZero			= ((uint16_t)((leftZero + rightZero) * 0.5));
//    //diagAvgZero		= (diagLeftZero + diagRightZero) * 0.5;

    //Odpalenie silnikow
    Motors_Init();
    DistSensorTimerStart();
    //Motor_Right_PWM(55);
    //Motor_Left_PWM(50);

    int iteracje=0;
    while(RX_data[0] != 'q') {

        //Niewielka filtracja
        encoderRight = ((htim3.Instance->CNT) / 4);
        encoderLeft = ((htim4.Instance->CNT) / 4);
//        if(iteracje < 10)
//        {
//          iteracje = iteracje +1;
//            //Kalibraca (pozycja startowa)
//            leftZero		= leftSMA;
//            rightZero		= rightSMA;
//            //diagLeftZero	= DistSensorSMAStep(&diagLeftSensor,  &diagLeftSMAsum, ADC_DIST_LEFT_DIAGONAL);
//            //diagRightZero	= DistSensorSMAStep(&diagRightSensor, &diagRightSMAsum, ADC_DIST_RIGHT_DIAGONAL);
//            avgZero			= (uint16_t)((leftZero + rightZero) * 0.5);
//            //diagAvgZero		= (diagLeftZero + diagRightZero) * 0.5;
//
//        }

        // SCIANY OBUSTRONNE
        if (FLAG_WALL_FRONT) {
            //STOP
            break;
        }
        if (FLAG_WALL_LEFT && FLAG_WALL_RIGHT) {


            // SMA czujnika prawego i lewego
            //Wyznaczenie odleglosci od LEWEGO i PRAWEGO na podstawie ch-ki
            //Obliczenie uchybu od lewego i prawego do środka
            //Uśrednienie uchybu
            //Podanie uchybu na regulator;
//                rightSMA	    = DistSensorSMAStep((uint16_t*)&rightSensor, &rightSMAsum, ADC_DIST_RIGHT_SIDE);
//                leftSMA		    = DistSensorSMAStep((uint16_t*)&leftSensor, &leftSMAsum, ADC_DIST_LEFT_SIDE);
//				diagLeftSMA     = DistSensorSMAStep((uint16_t*)&diagLeftSensor, &diagLeftSMAsum, ADC_DIST_LEFT_DIAGONAL);
//				diagRightSMA    = DistSensorSMAStep((uint16_t*)&diagRightSensor, &diagRightSMAsum, ADC_DIST_RIGHT_DIAGONAL);
//
//				uint16_t srednia;
//				srednia = (uint16_t)((rightSMA + leftSMA) * 0.5);
//
//				err = (int)srednia - (int)avgZero;
//                //err = (int)leftZero - (int)leftSMA;
//                if (err < 0) { err = -err; }
//
//				//Uchyb w "prawo" dodatni, uchyb "w lewo" ujemny
//				//Ten odczyt ktory wiekszy wskazuje kierunek krecenia
//				if (diagLeftSMA > diagRightSMA) { err = -err; }
//
//
//                UART_Send_String("L & R | \t");
        }
            // SCIANA LEWA
        else if (FLAG_WALL_LEFT) {
            // SMA czunika lewego
            // Wywalenie odczytu ktory nie jest fajny
            //rightSensor[0] = DIST_WALL_DISTANCE;
            //Obliczenie uchybu od lewego do środka
            //Uśrednienie uchybu
            //Podanie uchybu na regulator

//                rightSMA	 = 0;
//                leftSMA		 = DistSensorSMAStep((uint16_t*)&leftSensor, &leftSMAsum, ADC_DIST_LEFT_SIDE);
//				diagLeftSMA  = DistSensorSMAStep((uint16_t*)&diagLeftSensor, &diagLeftSMAsum, ADC_DIST_LEFT_DIAGONAL);
//				diagRightSMA = DistSensorSMAStep((uint16_t*)&diagRightSensor, &diagRightSMAsum, ADC_DIST_RIGHT_DIAGONAL);
//
//				err = (int)leftSMA - (int)leftZero;
//				if (err < 0) { err = -err; }
//
//				//Uchyb w "prawo" dodatni, uchyb "w lewo" ujemny
//				//Ten odczyt ktory wiekszy wskazuje kierunek krecenia
//				if (diagLeftSMA > diagRightSMA) { err = -err; }
//
//                UART_Send_String("L & _ | \t");
        }
            // SCIANA PRAWA
        else if (FLAG_WALL_RIGHT) {
            // SMA czujnika prawego
            // Wywalenie odczytu ktory nie jest fajny
            //leftSensor[0] = DIST_WALL_DISTANCE;
            //Obliczenie uchybu od prawego do środka
            //Uśrednienie uchybu
            //Podanie uchybu na regulator

//                rightSMA	= DistSensorSMAStep((uint16_t*)&rightSensor, &rightSMAsum, ADC_DIST_RIGHT_SIDE);
//                leftSMA		= 0;
//				diagLeftSMA = DistSensorSMAStep((uint16_t*)&diagLeftSensor, &diagLeftSMAsum, ADC_DIST_LEFT_DIAGONAL);
//				diagRightSMA = DistSensorSMAStep((uint16_t*)&diagRightSensor, &diagRightSMAsum, ADC_DIST_RIGHT_DIAGONAL);
//
//				err = (int)rightSMA - (int)rightZero;
//				if (err < 0) { err = -err; }
//
//				//Uchyb w "prawo" dodatni, uchyb "w lewo" ujemny
//				//Ten odczyt ktory wiekszy wskazuje kierunek krecenia
//				if (diagLeftSMA > diagRightSMA) { err = -err; }
//
//                UART_Send_String("_ & R | \t");
        }
            // BRAK SCIAN
        else {
//                //Jazda bez pida/pid oparty na żyroskopie
//                //rightSensor[0] = DIST_WALL_DISTANCE;
//                //leftSensor[0] = DIST_WALL_DISTANCE;
//                //Uchyb = 0
//                //Jazda prosto
//                rightSMA	= 0;
//                leftSMA		= 0;
//				//diagLeftSMA = 0;
//				//diagRightSMA = 0;
//
//				err = 0;
//
//                UART_Send_String("_ & _ | \t");
//                //GIT GUT
        }

//        if(err < 0){
//            err = -err;
//            err = (int)log((double)err);
//            err = -err;
//        } else{ err = (int)log((double)err); }

//        if(err < 0){
//            err = -err;
//            err = (int)sqrt((double)err);
//            err = -err;
//        } else{ err = (int)sqrt((double)err); }
        if (err > 10) { err = 10; }
        if (err < -10) { err = -10; }

//        TX_size = (uint16_t)sprintf((char*)TX_data, "SMA_Left=%i \tLeftZero=%i \terr=%i", leftSMA,leftZero, err);
        //TX_size = sprintf(TX_data, "SMA_Left=%i \t[0]=%i \t[1]=%i \t[2]=%i \t[3]=%i \t[4]=%i \tSMAsum=%i\n\r", leftSMA, leftSensor[0], leftSensor[1], leftSensor[2], leftSensor[3], leftSensor[4], leftSMAsum);
        //TX_size = sprintf(TX_data, "SMA_Right=%i \t[0]=%i \t[1]=%i \t[2]=%i \t[3]=%i \t[4]=%i \tSMAsum=%i\n\r", rightSMA, rightSensor[0], rightSensor[1], rightSensor[2], rightSensor[3], rightSensor[4], rightSMAsum);
//        HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
//        HAL_Delay(5);

//        TX_size = (uint16_t)sprintf((char*)TX_data, "\tSMA_Right=%i \tRightZer=%i \t||\t", rightSMA, rightZero);
//        HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
//        HAL_Delay(5);

        err = err + encoderLeft - encoderRight;

//        TX_size = (uint16_t)sprintf((char*)TX_data, "EN_L=%i \t\tPID_ERR=%i \t\tEN_R=%i\n\r", encoderLeft, err, encoderRight);
//        HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
        Motors_PID((float) err);
//        HAL_Delay(5);

        //podać błąd do PID-a

        //HAL_Delay(5);
    }
    Motors_Stop();
    DistSensorTimerStop();

    TX_size = (uint16_t)sprintf((char*)TX_data, "\tENC_LEFT=%i \tENC_RIGHT=%i \t||\t", (int)encoderLeft, (int)encoderRight);
    HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
    HAL_Delay(5);
}
void DEBUG_Soft_Motors_Rotate(void){
    UART_Send_String(">Selected: DEBUG_SOFT_Motors_Rotate\n\r");
    UART_Send_String("Press 'q' to exit.\n\r");

    int diff=0 , err;
    uint32_t encoderLeft, encoderRight;

    //Sterowanie silnikami
    //Motors_Init();
    //Turny maja wbudowane wszystko co ma Init.
    //DOCELWO - jeden Motors_Forward() i Turny() by nie uzywac Motors_Init!!!

    //Motors_Turn_Right(50);
    Motors_Turn_Left(50);

    while(RX_data[0] != 'q') {

        //OBROT - obliczamy roznice enkoderow ktora WZRASTA.
        encoderRight = ((htim3.Instance->CNT) / 4);
        encoderLeft  = ((htim4.Instance->CNT) / 4);

        diff = -(int)((encoderLeft - encoderRight));
        err = (208) - diff; //Tutaj - cel obrotu do ktorego dazymy;
        // 800 pelny obrot
        // 400 pol obrot
        // 200 90 stopni

        if(err <= 1){
            break;
        }
    }

    Motors_Stop();
    TX_size = (uint16_t)sprintf((char*)TX_data, "EN_L=%i \t\tdiff=%i \t\terr=%i \t\tEN_R=%i\n\r", encoderLeft, diff, err, encoderRight);
    HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
    HAL_Delay(5);
    //HAL_Delay(100);
    //Motors_Init();
    //DEBUG_Soft_Motors_PID();
}
void DEBUG_Soft_KalmanPosition(void){
    UART_Send_String(">Selected: DEBUG_SOFT_KalmanPosition\n\r");

    //Odpalenie silnikow
    Motors_Init();
//    Motor_Right_PWM(54);
//    Motor_Left_PWM(50);
    //HAL_Delay(100);

//    // 1) Zapisac odczyt czujnikow
//    // 2) Obrocic *recznie* robota
//    // 3) Puscic Kalmana
//    // 4) Czekac az dojedzie do ustalonej pozycji z pomocą PID-a.
//
//    //Czujnii odbicia boczne i frontalne
//	//uint16_t leftSensor[DIST_SMA_SAMPLES], rightSensor[DIST_SMA_SAMPLES];
//	uint16_t frontLSensor[DIST_SMA_SAMPLES], frontRSensor[DIST_SMA_SAMPLES];
//    //uint16_t leftSMAsum, rightSMAsum, rightSMA, leftSMA;
//    uint16_t frontLSMAsum, frontRSMAsum, frontLSMA, frontRSMA;
//    //uint16_t rightZero, leftZero;
//    uint16_t frontLZero, frontRZero, avgZero;
//
//    //Inicjalizacja tablic pomiarami
////    leftSMAsum		= DistSensorSMAInit(&leftSensor,  ADC_DIST_LEFT_SIDE);
////    rightSMAsum		= DistSensorSMAInit(&rightSensor, ADC_DIST_RIGHT_SIDE);
//    frontLSMAsum    = DistSensorSMAInit(&frontLSensor, ADC_DIST_LEFT_FORWARD);
//    frontRSMAsum    = DistSensorSMAInit(&frontRSensor, ADC_DIST_RIGHT_FORWARD);
////	diagLeftSMAsum	= DistSensorSMAInit(&diagLeftSensor, ADC_DIST_LEFT_DIAGONAL);
////	diagRightSMAsum = DistSensorSMAInit(&diagRightSensor, ADC_DIST_RIGHT_DIAGONAL);
////    rightSMA	    = DistSensorSMAStep(&rightSensor, &rightSMAsum, ADC_DIST_RIGHT_SIDE);
////    leftSMA		    = DistSensorSMAStep(&leftSensor, &leftSMAsum, ADC_DIST_LEFT_SIDE);
////    diagLeftSMA     = DistSensorSMAStep(&diagLeftSensor, &diagLeftSMAsum, ADC_DIST_LEFT_DIAGONAL);
////    diagRightSMA    = DistSensorSMAStep(&diagRightSensor, &diagRightSMAsum, ADC_DIST_RIGHT_DIAGONAL);
//    frontLSMA       = DistSensorSMAStep(&frontLSensor, &frontLSMAsum, ADC_DIST_LEFT_FORWARD);
//    frontRSMA       = DistSensorSMAStep(&frontRSensor, &frontRSMAsum, ADC_DIST_RIGHT_FORWARD);
//
//    frontLZero = frontLSMA;
//    frontRZero = frontRSMA;
//    avgZero = (frontLZero + frontRZero) * 0.5;
//
//    //Kalibraca (pozycja startowa)
//    //leftZero		= leftSMA;//DistSensorSMAStep(&leftSensor, &leftSMAsum, ADC_DIST_LEFT_SIDE);
//    //rightZero		= rightSMA;//DistSensorSMAStep(&rightSensor, &rightSMAsum, ADC_DIST_RIGHT_SIDE);
//    //diagLeftZero	= DistSensorSMAStep(&diagLeftSensor,  &diagLeftSMAsum, ADC_DIST_LEFT_DIAGONAL);
//    //diagRightZero	= DistSensorSMAStep(&diagRightSensor, &diagRightSMAsum, ADC_DIST_RIGHT_DIAGONAL);
//    //avgZero			= (leftZero + rightZero) * 0.5;
//    //diagAvgZero		= (diagLeftZero + diagRightZero) * 0.5;
//
//    Motors_Turn_Around();
    //routeEstimator();
}
void DEBUG_Soft_KalmanAngle(void){
    UART_Send_String(">Selected: DEBUG_SOFT_KalmanAngle\n\r");

    //Odpalenie silnikow DO SKRETU

    //Reset encoders
    htim3.Instance->CNT = 0;
    htim4.Instance->CNT = 0;

    //Turn on motors & set PWMs
    Motor_Right_TurnOn(MOTOR_FWD);
    Motor_Left_TurnOn(MOTOR_BWRD);
    Motor_Right_PWM(50);
    Motor_Left_PWM(50);

    //orientationEstimator();
}
void DEBUG_Soft_Mapping(void){
    UART_Send_String(">Selected: DEBUG_SOFT_Mapping\n\r");
}
void DEBUG_Soft_IMUProcessing(void){
    UART_Send_String(">Selected: DEBUG_SOFT_IMUProcessing\n\r");

    IMU_MPU6050_Init();

    //Odpalenie silnikow
    Motors_Init();
    Motor_Right_PWM(50);
    Motor_Left_PWM(50);

    while(RX_data[0] != 'q') {

        IMU_MPU6050_ReadRaw(((uint16_t*)&IMU_rawdata_acc),((int16_t*)&IMU_rawdata_gyro));
        TX_size = ((uint16_t)sprintf((char*)TX_data, "A_X = %i \t A_Y = %i \t A_Z = %i \t||\t G_R = %d \t G_P = %d \t G_Y = %d \t || ENC_L=%i \t ENC_R=%i\n\r", IMU_rawdata_acc[2], IMU_rawdata_acc[0], IMU_rawdata_acc[1], IMU_rawdata_gyro[2], IMU_rawdata_gyro[0], IMU_rawdata_gyro[1], (uint16_t)htim4.Instance->CNT, (uint16_t)htim3.Instance->CNT));
        HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
        HAL_Delay(10);
    }

    Motors_Stop();

}
void DEBUG_Soft_MemInfo(void){
    UART_Send_String(">Selected: DEBUG_SOFT_MemInfo\n\r");
    UART_Send_String("SIZES (IN BYTES):\n\r");
    TX_size = ((uint16_t)sprintf((char*)TX_data, "#int = %i\n\r#float = %i\n\r", sizeof(int), sizeof(float)));
    HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
    HAL_Delay(200);

    //Napisać funkcje do próby alokowania pamięci dla całej mapy! (16x16 kratek)
}
void DEBUG_Soft_DataExtractor(void){
    UART_Send_String(">Selected: DEBUG_SOFT_DataExtractor\n\r");
    //dataExtractor();

    //Napisać funkcje do próby alokowania pamięci dla całej mapy! (16x16 kratek)
}
