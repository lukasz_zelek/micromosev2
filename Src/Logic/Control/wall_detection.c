#include "Logic/Control/wall_detection.h"
#include "Foundations/led_model.h"

extern uint32_t* DistSensorWynik;

uint32_t DIST_WALL_DISTANCE_LEFT    = INITIAL_VALUE_CALLIBRATION;
uint32_t DIST_WALL_DISTANCE_RIGHT   = INITIAL_VALUE_CALLIBRATION;
uint32_t DIST_WALL_DISTANCE_FRONTR  = INITIAL_VALUE_CALLIBRATION;
uint32_t DIST_WALL_DISTANCE_FRONTL  = INITIAL_VALUE_CALLIBRATION;
uint32_t DIST_WALL_DISTANCE_DIAGR  = INITIAL_VALUE_CALLIBRATION;
uint32_t DIST_WALL_DISTANCE_DIAGL  = INITIAL_VALUE_CALLIBRATION;

//TODO USUNAC DIST_SENSOR_SMA
uint16_t DistSensorSMAInit(uint16_t* tab, uint16_t ADC_DIST_x){
    //Wystarczy przechowywac tylko DIST_SMA_SAMPLES+1 informacje:
    // Sume SMA_SMAPLES pomiarow oraz pomiary.
    int i, sum=0;
    for(i=0; i<DIST_SMA_SAMPLES; i++) {
        tab[i] = DistSensorMeassure(ADC_DIST_x);
        sum += tab[i];
        HAL_Delay(10);
    }
    return sum;

}

//TODO USUNAC DIST_SENSOR_SMAp
uint16_t DistSensorSMAStep(uint16_t* tab, uint16_t* sum, uint16_t ADC_DIST_x){
    int i;
    (*sum) = (*sum) - tab[DIST_SMA_SAMPLES - 1];
    for(i=DIST_SMA_SAMPLES -2; i>=0; i--) tab[i+1]=tab[i];
    tab[0] = DistSensorMeassure(ADC_DIST_x);
    (*sum) = (*sum) + tab[0];
    return (*sum)/DIST_SMA_SAMPLES;
}

void WallCheckCallibration(void){
    DistSensorTimerStart();
    HAL_Delay(500);
    DIST_WALL_DISTANCE_LEFT     = 3*LS(DistSensorWynik[LEFT_SIDE])/2;
    DIST_WALL_DISTANCE_FRONTL   = 11*LF(DistSensorWynik[LEFT_FORWARD])/10;
    DIST_WALL_DISTANCE_FRONTR   = 11*RF(DistSensorWynik[RIGHT_FORWARD])/10;
    DIST_WALL_DISTANCE_RIGHT    = 3*RS(DistSensorWynik[RIGHT_SIDE])/2;
    DistSensorTimerStop();
}

uint8_t WallCheck_Left(void) {
    if (LS(DistSensorWynik[LEFT_SIDE]) < DIST_WALL_DISTANCE_LEFT)//Wall detected - pomiar z ADC jest wiekszy od progowego
        return 1;
    return 0;
}

uint8_t WallCheck_Right(void) {
    if (RS(DistSensorWynik[RIGHT_SIDE]) < DIST_WALL_DISTANCE_RIGHT)//Wall detected - pomiar z ADC jest wiekszy od progowego
        return 1;
    return 0;
}

uint8_t WallCheck_Front(void) {

    //uint16_t srednia = (DistSensorMeassure(ADC_DIST_LEFT_FORWARD) + DistSensorMeassure(ADC_DIST_RIGHT_FORWARD)) / 2; - pomiar z ADC jest wiekszy od progowego

    if (RF(DistSensorWynik[RIGHT_FORWARD]) < DIST_WALL_DISTANCE_FRONTR && LF(DistSensorWynik[LEFT_FORWARD]) < DIST_WALL_DISTANCE_FRONTL) //Wall detected
        return 1;
    return 0;
}