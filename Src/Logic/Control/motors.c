#include "Logic/Control/motors.h"
#include "Foundations/led_model.h"
#include <math.h>
#include <Foundations/basic_io.h>

extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;

volatile uint16_t encoderRightCount; // Licznik impulsow
volatile uint16_t encoderRight; // Licznik przekreconych pozycji
volatile uint16_t encoderLeftCount; // Licznik impulsow
volatile uint16_t encoderLeft; // Licznik przekreconych pozycji

extern uint8_t   FLAG_WALL_FRONT;
extern uint8_t   FLAG_WALL_LEFT;
extern uint8_t   FLAG_WALL_RIGHT;
extern uint32_t *DistSensorWynik;
extern uint32_t DIST_WALL_DISTANCE_LEFT;
extern uint32_t DIST_WALL_DISTANCE_RIGHT;
extern uint32_t DIST_WALL_DISTANCE_FRONTR;
extern uint32_t DIST_WALL_DISTANCE_FRONTL;
extern uint32_t DIST_WALL_DISTANCE_DIAGR;
extern uint32_t DIST_WALL_DISTANCE_DIAGL;

int integral=0;

extern uint8_t  TX_data[UART_TX_BUFFOR_SIZE];
extern uint16_t TX_size;
extern UART_HandleTypeDef huart4;

void Motors_Init(void){
    //Turn on motors & set PWMs
    Motor_Right_TurnOn(MOTOR_FWD);
    Motor_Left_TurnOn(MOTOR_FWD);
    Motor_Right_PWM(0);
    Motor_Left_PWM(0);
    integral = 0;
    //Reset encoders
    htim3.Instance->CNT = 0;	//Wyzeruj pozycje enkodera
    htim4.Instance->CNT = 0;
}


void Motors_Forward(void) {
    //Ruch o jedna kratke

    uint32_t encoderRight, encoderLeft;
    int err = 0;
    Motors_Init();

    do{
        int wallerr = 0;

        encoderRight = (htim3.Instance->CNT) / 4;
        encoderLeft  = (htim4.Instance->CNT) / 4;

        if(LD(DistSensorWynik[LEFT_DIAGONAL]) < DIST_WALL_DISTANCE_DIAGL && RD(DistSensorWynik[RIGHT_DIAGONAL]) < DIST_WALL_DISTANCE_DIAGR)
            wallerr = LD(DistSensorWynik[LEFT_DIAGONAL]) - RD(DistSensorWynik[RIGHT_DIAGONAL]);

//        if(wallerr == 0)
//        {
//            if(LD(DistSensorWynik[LEFT_DIAGONAL]) < 90 && LF(DistSensorWynik[LEFT_FORWARD]) > 90)
//                wallerr += 50;
//            else if(RD(DistSensorWynik[RIGHT_DIAGONAL]) < 90 && RF(DistSensorWynik[RIGHT_FORWARD]) > 90)
//                wallerr -= 50;
//        }
//        if(wallerr == 0)
//        {
//            if(LS(DistSensorWynik[LEFT_SIDE]) < 300)
//                wallerr += 50;
//            else if(RS(DistSensorWynik[RIGHT_SIDE]) < 300)
//                wallerr -= 50;
//        }

        err = err + (int)(encoderLeft - encoderRight) + 0.2*wallerr;
        if (err > 10) { err = 10; }
        if (err < -10) { err = -10; }
        Motors_PID((float) err);

    }while((!FLAG_WALL_FRONT && ( encoderLeft < 290 && encoderRight < 290) ) || (encoderLeft > 1000) || (encoderRight > 1000));

    Motors_Stop();
    //LED_Tur
    // n_On(LED3_Pin);
    //HAL_Delay(10);
    //LED_Turn_Off(LED3_Pin);
}

void Motors_Backward(void) {
    htim3.Instance->CNT = 0;	//Wyzeruj pozycje enkodera
    htim4.Instance->CNT = 0;
    // __HAL_TIM_GET_COUNTER(&htim3);
    Motor_Right_TurnOn(MOTOR_BWRD);
    Motor_Left_TurnOn(MOTOR_BWRD);
    Motor_Right_PWM(100);
    Motor_Left_PWM(100);
    //while (1) {
        encoderRight = (htim3.Instance->CNT) / 4;
        encoderRightCount = TIM3->CNT;
        encoderLeft = (htim4.Instance->CNT) / 4;
        encoderLeftCount = TIM4->CNT;
        HAL_Delay(1000);
        HAL_Delay(500);
    //}
    Motor_Left_TurnOff();
    Motor_Right_TurnOff();
}

void Motors_Stop(void){
    Motor_Right_TurnOff();
    Motor_Left_TurnOff();
    Motor_Right_PWM(0);
    Motor_Left_PWM(0);
}

void Motors_PID(float error){

    const float Kp=PID_K, Ki=PID_I, Kd=PID_D; //wyjebany czlon I bo na wykladzie u Oprzeda bylo ze jest do silnikow niepotrzebny
    //const int offset = 0;
    static int lastError=0;
    int Turn, derivative;

    integral += error;
    derivative = error - lastError;
    Turn = (int)(Kp*error + Ki*integral + Kd*derivative);
    lastError = error;

    //Motor_Right_PWM(MOTORS_PWM_MAX - Turn);
    //Motor_Left_PWM (MOTORS_PWM_MAX + Turn);
    if(Turn > MOTOR_PWM_MAX){Turn =MOTOR_PWM_MAX;}
    if(Turn < -MOTOR_PWM_MAX){Turn =-MOTOR_PWM_MAX;}
    if(integral > MOTOR_PID_MAX){integral = MOTOR_PID_MAX;}
    if(integral < -MOTOR_PID_MAX){integral = -MOTOR_PID_MAX;}

    //TX_size = sprintf(TX_data, "\tTurn=%i \tInt=%i \tdErr=%i\n\r", Turn, integral, derivative);
    //HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
    //HAL_Delay(2);

    Motor_Right_PWM(MOTOR_PWM_RIGHT + Turn); //85
    Motor_Left_PWM (MOTOR_PWM_LEFT  - Turn); //100
}

void Motors_Turn_Left(uint16_t duty) {
    Motors_Init();
    __HAL_TIM_GET_COUNTER(&htim3) = 32768;
    __HAL_TIM_GET_COUNTER(&htim4) = 32768;
    Motor_Right_TurnOn(MOTOR_FWD);
    Motor_Left_TurnOn(MOTOR_BWRD);
    Motor_Right_PWM(duty+15);
    Motor_Left_PWM(duty);
}

void Motors_Turn_Left90Degrees(void) {

    int  err=500;
    uint32_t encoderLeft, encoderRight;

    Motors_Turn_Left(100);

    while(err > 1){
        encoderRight = (htim3.Instance->CNT) / 4;
        encoderLeft = (htim4.Instance->CNT) / 4;
        err = (209) - (int)(encoderRight - encoderLeft);
    }
    Motors_Stop();
}

void Motors_Turn_Right(uint16_t duty) {
    Motors_Init();
    htim3.Instance->CNT = 32768;
    htim4.Instance->CNT = 32768;
    Motor_Right_TurnOn(MOTOR_BWRD);
    Motor_Left_TurnOn(MOTOR_FWD);
    Motor_Right_PWM(duty+15);
    Motor_Left_PWM(duty);
}


void Motors_Turn_Right90Degrees(void) {
    int err = 500;
    uint32_t encoderLeft, encoderRight;

    Motors_Turn_Right(100);

    while (err > 1){
        encoderRight = (htim3.Instance->CNT) / 4;
        encoderLeft = (htim4.Instance->CNT) / 4;
        err = (210) - (int)(encoderLeft - encoderRight);
    }
    Motors_Stop();
}

void Motor_Left_Move(uint16_t encoder_counts) {
    htim4.Instance->CNT = 0;	//Wyzeruj pozycje enkodera
    Motor_Left_TurnOn(MOTOR_FWD);
    Motor_Left_PWM(200);
    //Dzielimy przez cztery, gdyz jedna pozycja generuje 4 ticki enkodera
    while ((htim4.Instance->CNT) / 4 < encoder_counts) {
    }
    Motor_Left_TurnOff();
}
void Motor_Right_Move(uint16_t encoder_counts) {
    htim3.Instance->CNT = 0;	//Wyzeruj pozycje enkodera
    Motor_Right_TurnOn(MOTOR_FWD);
    Motor_Right_PWM(200);
//Dzielimy przez cztery, gdyz jedna pozycja generuje 4 ticki enkodera
    while ((htim3.Instance->CNT) / 4 < encoder_counts) {
    }
    Motor_Right_TurnOff();
}
void Motors_Turn_Around(void) {
//    int diff = 0;
//    uint16_t encoderLeft, encoderRight;
//
//    Motors_Turn_Right(50);
//
//    while (410 > diff){
//        encoderRight = (htim3.Instance->CNT) / 4;
//        encoderLeft = (htim4.Instance->CNT) / 4;
//        diff = encoderLeft - encoderRight;
//    }
//    Motors_Stop();
    Motors_Turn_Right90Degrees();
    HAL_Delay(5);
    Motors_Turn_Right90Degrees();
}
void Motors_ForwardCertainTimes(int revolutionCount, int dutyPercentage) {
    htim3.Instance->CNT = 0;	//Wyzeruj pozycje enkodera
    htim4.Instance->CNT = 0;
    Motor_Right_TurnOn(MOTOR_FWD);
    Motor_Left_TurnOn(MOTOR_FWD);
    Motor_Right_PWM(dutyPercentage * 10);
    Motor_Left_PWM(dutyPercentage * 10);
//Dzielimy przez cztery, gdyz jedna pozycja generuje 4 ticki enkodera
    int revolutionTicks = 51 * 4 * 3 * revolutionCount;
    while ((htim3.Instance->CNT) < revolutionTicks
           && (htim4.Instance->CNT) < revolutionTicks) {
        encoderRight = (htim3.Instance->CNT) / 4;
        encoderRightCount = TIM3->CNT;
        encoderLeft = (htim4.Instance->CNT) / 4;
        encoderLeftCount = TIM4->CNT;
    }
    Motor_Left_TurnOff();
    Motor_Right_TurnOff();
}
void Motors_BackwardCertainTimes(int revolutionCount, int dutyPercentage) {
    htim3.Instance->CNT = 65535;	//Wyzeruj pozycje enkodera
    htim4.Instance->CNT = 65535;
    Motor_Right_TurnOn(MOTOR_BWRD);
    Motor_Left_TurnOn(MOTOR_BWRD);
    Motor_Right_PWM(dutyPercentage * 10);
    Motor_Left_PWM(dutyPercentage * 10);
//Dzielimy przez cztery, gdyz jedna pozycja generuje 4 ticki enkodera
    int revolutionTicks = 50 * 4 * 3 * revolutionCount;
    while ((htim3.Instance->CNT) > 65535 - revolutionTicks
           && (htim4.Instance->CNT) > 65535 - revolutionTicks) {
        encoderRight = (htim3.Instance->CNT) / 4;
        encoderRightCount = TIM3->CNT;
        encoderLeft = (htim4.Instance->CNT) / 4;
        encoderLeftCount = TIM4->CNT;
    }
    Motor_Left_TurnOff();
    Motor_Right_TurnOff();
}