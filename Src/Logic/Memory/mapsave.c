//
// Created by Mad Scientist on 06.04.2019.
//
#include "Logic/Memory/mapsave.h"

void Memory_MapSave(microMouseState* savedata){

    int sufix = 0x0000;
    //uint8_t flooding[N][N];
    for(int i=0; i<MAP_SIZE_N; i++){
        for(int j=0; j<MAP_SIZE_N; j++)
        {
            EEPROM_Write((uint16_t) (sufix + 2*(i * MAP_SIZE_N + j)), (uint8_t *) &(savedata->flooding[i][j]));
            if(i == MAP_SIZE_N - 1 && j == MAP_SIZE_N - 1)
                sufix = (sufix + 2*(i * MAP_SIZE_N + j))+2;
        }
    }
    //uint8_t mappedArea[N][N];
    for(int i=0; i<MAP_SIZE_N; i++){
        for(int j=0; j<MAP_SIZE_N; j++)
        {
            EEPROM_Write((uint16_t) (sufix + 2*(i * MAP_SIZE_N + j)), (uint8_t *) &(savedata->mappedArea[i][j]));
            if(i == MAP_SIZE_N - 1 && j == MAP_SIZE_N - 1)
                sufix = (sufix + 2*(i * MAP_SIZE_N + j))+2;
        }
    }
    //uint8_t visited[0xff+1];
    for(int i=0; i < MAP_SIZE_NN; i++){
        EEPROM_Write((uint16_t)(sufix + 2*i),(uint8_t*)&(savedata->visited[i]));
        if(i == MAP_SIZE_NN - 1)
            sufix = (sufix + 2*i)+2;
    }
    //int next_to_explore;
    uint8_t tabint[4];
    tabint[0] = (uint8_t)((savedata->nextToExplore) >> 0);
    tabint[1]= (uint8_t)((savedata->nextToExplore) >> 8);
    tabint[2] = (uint8_t)((savedata->nextToExplore) >> 16);
    tabint[3] = (uint8_t)((savedata->nextToExplore) >> 24);

    for(int i=0; i<4; i++){
        EEPROM_Write((uint16_t)(sufix + 2*i),(uint8_t*)&(tabint[i]));
        if(i == 3)
            sufix = (sufix + 2*i) + 2;
    }
    //int step;
    tabint[0] = (uint8_t)((savedata->step) >> 0);
    tabint[1]= (uint8_t)((savedata->step) >> 8);
    tabint[2] = (uint8_t)((savedata->step) >> 16);
    tabint[3] = (uint8_t)((savedata->step) >> 24);

    for(int i=0; i<4; i++){
        EEPROM_Write((uint16_t)(sufix + 2*i),(uint8_t*)&(tabint[i]));
        if(i == 3)
            sufix = (sufix + 2*i) + 2;
    }
    //int target;
    tabint[0] = (uint8_t)((savedata->target) >> 0);
    tabint[1]= (uint8_t)((savedata->target) >> 8);
    tabint[2] = (uint8_t)((savedata->target) >> 16);
    tabint[3] = (uint8_t)((savedata->target) >> 24);

    for(int i=0; i<4; i++){
        EEPROM_Write((uint16_t)(sufix + 2*i),(uint8_t*)&(tabint[i]));
        if(i == 3)
            sufix = (sufix + 2*i) + 2;
    }
    //uint8_t mmx;
    EEPROM_Write((uint16_t)(sufix),&(savedata->x));
    sufix += 2;
    //uint8_t mmy;
    EEPROM_Write((uint16_t)(sufix),&(savedata->y));
    sufix += 2;
    //uint8_t mmr;
    EEPROM_Write((uint16_t)(sufix),&(savedata->rotation));
    sufix += 2;
    //uint8_t actual_visit_state;
    EEPROM_Write((uint16_t)(sufix),&(savedata->actualVisitState));
    sufix += 2;
    //uint8_t actual_sensors_state;
    EEPROM_Write((uint16_t)(sufix),&(savedata->actualSensorsState));
    sufix += 2;
    //int8_t righthanded;
    EEPROM_Write((uint16_t)(sufix),(uint8_t*)&(savedata->mazeTypeRightHanded));
    sufix += 2;
    //int8_t nodeFound;
    EEPROM_Write((uint16_t)(sufix),(uint8_t*)&(savedata->nodeFound));
    sufix += 2;
    //int8_t pathReturn;
    EEPROM_Write((uint16_t)(sufix),(uint8_t*)&(savedata->pathReturn));
    sufix += 2;
    //int8_t mapingFinished;
    EEPROM_Write((uint16_t)(sufix),(uint8_t*)&(savedata->mapIsFinished));
}

void Memory_MapLoad(microMouseState* loaddata){

    int sufix = 0x0000;
    //uint8_t flooding[N][N];
    for(int i=0; i<MAP_SIZE_N; i++){
        for(int j=0; j<MAP_SIZE_N; j++)
        {
            EEPROM_Read((uint16_t) (sufix + 2*(i * MAP_SIZE_N + j)), (uint8_t *) &(loaddata->flooding[i][j]));
            if(i == MAP_SIZE_N - 1 && j == MAP_SIZE_N - 1)
                sufix = (sufix + 2*(i * MAP_SIZE_N + j))+2;
        }
    }
    //uint8_t mappedArea[N][N];
    for(int i=0; i<MAP_SIZE_N; i++){
        for(int j=0; j<MAP_SIZE_N; j++)
        {
            EEPROM_Read((uint16_t) (sufix + 2*(i * MAP_SIZE_N + j)), (uint8_t *) &(loaddata->mappedArea[i][j]));
            if(i == MAP_SIZE_N - 1 && j == MAP_SIZE_N - 1)
                sufix = (sufix + 2*(i * MAP_SIZE_N + j))+2;
        }
    }
    //uint8_t visited[0xff+1];
    for(int i=0; i < MAP_SIZE_NN; i++){
        EEPROM_Read((uint16_t)(sufix + 2*i),(uint8_t*)&(loaddata->visited[i]));
        if(i == MAP_SIZE_NN - 1)
            sufix = (sufix + 2*i)+2;
    }
    //int next_to_explore;
    uint8_t tabint[4];

    for(int i=0; i<4; i++){
        EEPROM_Read((uint16_t)(sufix + 2*i),(uint8_t*)&(tabint[i]));
        if(i == 3)
            sufix = (sufix + 2*i) + 2;
    }
    (loaddata->nextToExplore) = tabint[3];
    (loaddata->nextToExplore) <<= 8;
    (loaddata->nextToExplore) += tabint[2];
    (loaddata->nextToExplore) <<= 8;
    (loaddata->nextToExplore) += tabint[1];
    (loaddata->nextToExplore) <<= 8;
    (loaddata->nextToExplore) += tabint[0];


    //int step;
    for(int i=0; i<4; i++){
        EEPROM_Read((uint16_t)(sufix + 2*i),(uint8_t*)&(tabint[i]));
        if(i == 3)
            sufix = (sufix + 2*i) + 2;

        (loaddata->step) = tabint[3];
        (loaddata->step) <<= 8;
        (loaddata->step) += tabint[2];
        (loaddata->step) <<= 8;
        (loaddata->step) += tabint[1];
        (loaddata->step) <<= 8;
        (loaddata->step) += tabint[0];
    }
    //int target;

    for(int i=0; i<4; i++){
        EEPROM_Read((uint16_t)(sufix + 2*i),(uint8_t*)&(tabint[i]));
        if(i == 3)
            sufix = (sufix + 2*i) + 2;
    }
    (loaddata->target) = tabint[3];
    (loaddata->target) <<= 8;
    (loaddata->target) += tabint[2];
    (loaddata->target) <<= 8;
    (loaddata->target) += tabint[1];
    (loaddata->target) <<= 8;
    (loaddata->target) += tabint[0];
    //uint8_t mmx;
    EEPROM_Read((uint16_t)(sufix),&(loaddata->x));
    sufix += 2;
    //uint8_t mmy;
    EEPROM_Read((uint16_t)(sufix),&(loaddata->y));
    sufix += 2;
    //uint8_t mmr;
    EEPROM_Read((uint16_t)(sufix),&(loaddata->rotation));
    sufix += 2;
    //uint8_t actual_visit_state;
    EEPROM_Read((uint16_t)(sufix),&(loaddata->actualVisitState));
    sufix += 2;
    //uint8_t actual_sensors_state;
    EEPROM_Read((uint16_t)(sufix),&(loaddata->actualSensorsState));
    sufix += 2;
    //int8_t righthanded;
    EEPROM_Read((uint16_t)(sufix),(uint8_t*)&(loaddata->mazeTypeRightHanded));
    sufix += 2;
    //int8_t nodeFound;
    EEPROM_Read((uint16_t)(sufix),(uint8_t*)&(loaddata->nodeFound));
    sufix += 2;
    //int8_t pathReturn;
    EEPROM_Read((uint16_t)(sufix),(uint8_t*)&(loaddata->pathReturn));
    sufix += 2;
    //int8_t mapingFinished;
    EEPROM_Read((uint16_t)(sufix),(uint8_t*)&(loaddata->mapIsFinished));
}