/*
* marix.cpp
*
*  Created on: 11 Mar 2018
*      Author: ilona
*/
#include "Logic/Kalman_filter/matrix.h"

Matrix allocateMatrix(void)
{
    Matrix mat;
    mat = (Matrix)malloc(sizeof(Vector) * VRISKERMATRIX_SIZE);

    if (mat == NULL) return NULL;
    mat[0] = allocateVector();
    if (mat[0] == NULL) { free(mat); return NULL; }
    mat[1] = allocateVector();
    if (mat[1] == NULL) { free(mat[1]); free(mat); return NULL; }

    return mat;
}
Vector allocateVector(void) {

    Vector vec;
    vec = (Vector)malloc(sizeof(Scalar) * VRISKERVECTOR_SIZE);
    if (vec == NULL) return NULL;

    vec[0] = 0;
    vec[1] = 0;

    return vec;
}

void setMatrix(Matrix mat, Scalar val00, Scalar val01, Scalar val10, Scalar val11) {

    if (mat == NULL) return;

    mat[0][0] = val00;
    mat[0][1] = val01;
    mat[1][0] = val10;
    mat[1][1] = val11;

}
void setVector(Vector vec, Scalar val0, Scalar val1) {

    if (vec == NULL) return;
    vec[0] = val0;
    vec[1] = val1;
}
void freeMatrix(Matrix* mat) {

    if (*mat == NULL) return;

    free((*mat)[1]);
    free((*mat)[0]);
    free(*mat);
    *mat = NULL;
    return;
}
void freeVector(Vector* vec) {

    if (*vec == NULL) return;
    free(*vec);
    *vec = 0;
    return;
}

void matrixAdd(Matrix in1, Matrix in2, Matrix acc) {

    if (in1 == NULL || in2 == NULL || acc == NULL) return;

    acc[0][0] = in1[0][0] + in2[0][0];
    acc[0][1] = in1[0][1] + in2[0][1];
    acc[1][0] = in1[1][0] + in2[1][0];
    acc[1][1] = in1[1][1] + in2[1][1];
}
void vectorAdd(Vector in1, Vector in2, Vector acc) {

    if (in1 == NULL || in2 == NULL || acc == NULL) return;

    acc[0] = in1[0] + in2[0];
    acc[1] = in1[1] + in2[1];
}
void matrixSub(Matrix in1, Matrix in2, Matrix acc) {

    if (in1 == NULL || in2 == NULL || acc == NULL) return;

    acc[0][0] = in1[0][0] - in2[0][0];
    acc[0][1] = in1[0][1] - in2[0][1];
    acc[1][0] = in1[1][0] - in2[1][0];
    acc[1][1] = in1[1][1] - in2[1][1];
}
void vectorSub(Vector in1, Vector in2, Vector acc) {

    if (in1 == NULL || in2 == NULL || acc == NULL) return;
    acc[0] = in1[0] - in2[0];
    acc[1] = in1[1] - in2[1];
}
void matrixTranspose(Matrix mat, Matrix acc) {

    if (mat == NULL || acc == NULL) return;

    acc[0][0] = mat[0][0];
    acc[0][1] = mat[1][0];
    acc[1][0] = mat[0][1];
    acc[1][1] = mat[1][1];
}
void matrixMultScalar(Matrix in1, Scalar in2, Matrix acc) {

    if (in1 == NULL || acc == NULL) return;

    acc[0][0] = in1[0][0] * in2;
    acc[0][1] = in1[0][1] * in2;
    acc[1][0] = in1[1][0] * in2;
    acc[1][1] = in1[1][1] * in2;
}
void vectorMultScalar(Vector in1, Scalar in2, Vector acc) {

    if (in1 == NULL || acc == NULL) return;

    acc[0] = in1[0] * in2;
    acc[1] = in1[1] * in2;
}
void matrixMultVector(Matrix in1, Vector in2, Vector acc) {

    if (in1 == NULL || in2 == NULL || acc == NULL) return;

    acc[0] = in1[0][0] * in2[0] + in1[0][1] * in2[1];
    acc[1] = in1[1][0] * in2[0] + in1[1][1] * in2[1];
}
void matrixMultMatrix(Matrix in1, Matrix in2, Matrix acc) {

    if (in1 == NULL || in2 == NULL || acc == NULL) return;

    acc[0][0] = in1[0][0] * in2[0][0] + in1[0][1] * in2[1][0];
    acc[0][1] = in1[0][0] * in2[0][1] + in1[0][1] * in2[1][1];
    acc[1][0] = in1[1][0] * in2[0][0] + in1[1][1] * in2[1][0];
    acc[1][1] = in1[1][0] * in2[0][1] + in1[1][1] * in2[1][1];
}
void rowVectorMultMarix(RowVector in1, Matrix in2, RowVector acc) {

    if (in1 == NULL || in2 == NULL || acc == NULL) return;

    acc[0] = in1[0] * in2[0][0] + in1[1] * in2[1][0];
    acc[1] = in1[0] * in2[0][1] + in1[1] * in2[1][1];
}
Scalar rowVectorMultVector(RowVector in1, Vector in2) {

    if (in1 == NULL || in2 == NULL) return -1;

    return in1[0] * in2[0] + in1[1] * in2[1];
}
void vectorMultRowVector(Vector in1, RowVector in2, Matrix acc) {

    if (in1 == NULL || in2 == NULL) return;

    acc[0][0] = in1[0] * in2[0];
    acc[0][1] = in1[0] * in2[1];
    acc[1][0] = in1[1] * in2[0];
    acc[1][1] = in1[1] * in2[1];
}
