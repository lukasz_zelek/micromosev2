//
// Created by mad on 09.01.19.
//

#include "Foundations/imu.h"

extern I2C_HandleTypeDef hi2c2;


void IMU_MPU6050_Init(void){
    MPU6050_SetClockSource(MPU6050_CLOCK_PLL_XGYRO);
    MPU6050_SetFullScaleGyroRange(MPU6050_GYRO_FS_250);
    MPU6050_SetFullScaleAccelRange(MPU6050_ACCEL_FS_2);
    MPU6050_SetSleepModeStatus(DISABLE);
}

void IMU_MPU6050_ReadRaw(uint16_t* raw_acc, int16_t* raw_gyro){
    uint8_t tmpBuffer[14];
    MPU6050_I2C_BufferRead(MPU6050_DEFAULT_ADDRESS, tmpBuffer, MPU6050_RA_ACCEL_XOUT_H, 14);
    /* Get acceleration */
    for (int i = 0; i < 3; i++)
        raw_acc[i] = ((uint16_t) ((uint16_t) tmpBuffer[2 * i] << 8) + tmpBuffer[2 * i + 1]);
    /* Get Angular rate */
    for (int i = 4; i < 7; i++)
        raw_gyro[i - 4] = ((int16_t) ((uint16_t) tmpBuffer[2 * i] << 8) + tmpBuffer[2 * i + 1]);
}



// TODO PRZEKONWETOWAĆ BIBLIOTEKĘ DO NASZYCH POTRZEB!