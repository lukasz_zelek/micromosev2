//
// Created by Mad Scientist on 06.04.2019.
//
#include "Foundations/trig_series.h"

double Math_Trig_Series_sin(double x){
        return -0.1667*x*x*x + x;
}

double Math_Trig_Series_cos(double x){
    return -0.5*x*x + 1;
}

double Math_Trig_Series_tan(double x){
    return 0.3333*x*x*x + x;
}

double Math_Trig_Series_arcsin(double x){
    return 0.1667*x*x*x + x;
}

double Math_Trig_Series_arctan(double x){
    return -0.3333*x*x*x+x;
}