//
// Created by mad on 17.12.18.
//

#include "Foundations/basic_io.h"

extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc3;
extern DMA_HandleTypeDef hdma_adc1;
extern I2C_HandleTypeDef hi2c2;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern TIM_HandleTypeDef htim6;
extern TIM_HandleTypeDef htim7;
extern UART_HandleTypeDef huart4;

extern uint8_t  TX_data[UART_TX_BUFFOR_SIZE];
extern uint16_t TX_size;




void LED_Turn_OnAll(void) {

    HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin, GPIO_PIN_SET);

}



void LED_Turn_On(uint16_t LEDx_Pin) {

    if (LEDx_Pin == LED1_Pin) {
        HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
    } else if (LEDx_Pin == LED2_Pin) {
        HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET);
    } else if (LEDx_Pin == LED3_Pin) {
        HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_SET);
    } else if (LEDx_Pin == LED4_Pin) {
        HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin, GPIO_PIN_SET);
    }
}



void LED_Toggle(uint16_t LEDx_Pin) {

    if (LEDx_Pin == LED1_Pin) {
        HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);
    } else if (LEDx_Pin == LED2_Pin) {
        HAL_GPIO_TogglePin(LED2_GPIO_Port, LED2_Pin);
    } else if (LEDx_Pin == LED3_Pin) {
        HAL_GPIO_TogglePin(LED3_GPIO_Port, LED3_Pin);
    } else if (LEDx_Pin == LED4_Pin) {
        HAL_GPIO_TogglePin(LED4_GPIO_Port, LED4_Pin);
    }
}



void LED_Turn_OffAll(void) {

    HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin, GPIO_PIN_RESET);
}



void LED_Turn_Off(uint16_t LEDx_Pin) {
    if (LEDx_Pin == LED1_Pin) {
        HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
    } else if (LEDx_Pin == LED2_Pin) {
        HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
    } else if (LEDx_Pin == LED3_Pin) {
        HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_RESET);
    } else if (LEDx_Pin == LED4_Pin) {
        HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin, GPIO_PIN_RESET);
    }
}



GPIO_PinState BTN_IsPressed(uint16_t BTNx_Pin) {
    if (BTNx_Pin == BTN1_Pin) {
        return !HAL_GPIO_ReadPin(BTN1_GPIO_Port, BTN1_Pin);
    } else if (BTNx_Pin == BTN2_Pin) {
        return !HAL_GPIO_ReadPin(BTN2_GPIO_Port, BTN2_Pin);
    } else if (BTNx_Pin == BTN3_Pin) {
        return !HAL_GPIO_ReadPin(BTN3_GPIO_Port, BTN3_Pin);
    }
    return GPIO_PIN_RESET;
}



float Battery_Voltage_Measure(){

    uint16_t PomiarADC;

    HAL_ADC_Start(&hadc3);

    while (HAL_ADC_PollForConversion(&hadc3, 10) != HAL_OK) {}
        PomiarADC = HAL_ADC_GetValue(&hadc3);

    return (2.4 / (3555 - 2675) * (PomiarADC - 2675) + 6);
}



void UART_Send_String(char* String) {

    TX_size = sprintf(TX_data, String);
    HAL_UART_Transmit_IT(&huart4, TX_data, TX_size);
    HAL_Delay(2);
}