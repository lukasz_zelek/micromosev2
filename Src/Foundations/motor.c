//
// Created by mad on 09.01.19.
//

#include "Foundations/motor.h"

extern TIM_HandleTypeDef htim2;

// Nisko poziomowe sterowanie silnikami
void Motor_Left_TurnOn(MOTOR_Direction dir) {

    HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
    if (dir == MOTOR_FWD) {
        HAL_GPIO_WritePin(MOTOR_LEFT_FWD_GPIO_Port, MOTOR_LEFT_FWD_Pin,
                          GPIO_PIN_SET);
        HAL_GPIO_WritePin(MOTOR_LEFT_BWRD_GPIO_Port, MOTOR_LEFT_BWRD_Pin,
                          GPIO_PIN_RESET);
    } else {
        HAL_GPIO_WritePin(MOTOR_LEFT_FWD_GPIO_Port, MOTOR_LEFT_FWD_Pin,
                          GPIO_PIN_RESET);
        HAL_GPIO_WritePin(MOTOR_LEFT_BWRD_GPIO_Port, MOTOR_LEFT_BWRD_Pin,
                          GPIO_PIN_SET);
    }
}
void Motor_Left_TurnOff(void) {
    HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_2);
    HAL_GPIO_WritePin(MOTOR_LEFT_FWD_GPIO_Port, MOTOR_LEFT_FWD_Pin,
                      GPIO_PIN_RESET);
    HAL_GPIO_WritePin(MOTOR_LEFT_BWRD_GPIO_Port, MOTOR_LEFT_BWRD_Pin,
                      GPIO_PIN_RESET);

}
void Motor_Right_TurnOn(MOTOR_Direction dir) {
    HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);
    if (dir == MOTOR_FWD) {
        HAL_GPIO_WritePin(MOTOR_RIGHT_FWD_GPIO_Port, MOTOR_RIGHT_FWD_Pin,
                          GPIO_PIN_SET);
        HAL_GPIO_WritePin(MOTOR_RIGHT_BWRD_GPIO_Port, MOTOR_RIGHT_BWRD_Pin,
                          GPIO_PIN_RESET);
    } else {
        HAL_GPIO_WritePin(MOTOR_RIGHT_FWD_GPIO_Port, MOTOR_RIGHT_FWD_Pin,
                          GPIO_PIN_RESET);
        HAL_GPIO_WritePin(MOTOR_RIGHT_BWRD_GPIO_Port, MOTOR_RIGHT_BWRD_Pin,
                          GPIO_PIN_SET);
    }
}
void Motor_Right_TurnOff(void) {
    HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_3);
    HAL_GPIO_WritePin(MOTOR_RIGHT_FWD_GPIO_Port, MOTOR_RIGHT_FWD_Pin,
                      GPIO_PIN_RESET);
    HAL_GPIO_WritePin(MOTOR_RIGHT_BWRD_GPIO_Port, MOTOR_RIGHT_BWRD_Pin,
                      GPIO_PIN_RESET);
}
void Motor_Right_PWM(uint16_t duty) {
    htim2.Instance->CCR2 = duty;
}
void Motor_Left_PWM(uint16_t duty) {
    htim2.Instance->CCR3 = duty;
}