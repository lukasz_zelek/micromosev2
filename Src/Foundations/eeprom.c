//
// Created by mad on 09.01.19.
//

#include "Foundations/eeprom.h"

extern I2C_HandleTypeDef hi2c2;


void EEPROM_Write(uint16_t addr, uint8_t* data){
    HAL_I2C_Mem_Write(&hi2c2, EEPROM_ADDRESS, addr, EEPROM_MEMADD_SIZE_16BIT, data, 1, 100);
    //TODO Tester-debug - DO USUNIECIA W PRZYSZLOSC
    HAL_Delay(1);
}



void EEPROM_Write_Bytes(uint16_t addr, uint8_t* pData, uint8_t size) {
    if(size > 64) {
        int n = size % 64;
        int i;
        for (i = 0; i < n; i++) {
            HAL_I2C_Mem_Write(&hi2c2, EEPROM_ADDRESS, addr, EEPROM_MEMADD_SIZE_16BIT, pData + i * 64, 64, 100);
        }
        HAL_I2C_Mem_Write(&hi2c2, EEPROM_ADDRESS, addr, EEPROM_MEMADD_SIZE_16BIT, pData + n * 64, size - n*64, 100);
    }
    else{
        HAL_I2C_Mem_Write(&hi2c2, EEPROM_ADDRESS, addr, EEPROM_MEMADD_SIZE_16BIT, pData, size, 100);
    }

}



void EEPROM_Read(uint16_t addr, uint8_t* buffer){
    HAL_I2C_Mem_Read(&hi2c2,  EEPROM_ADDRESS, addr, EEPROM_MEMADD_SIZE_16BIT, buffer, 1, 100);
}



void EEPROM_Read_Bytes(uint16_t addr, uint8_t* pBuffer, uint8_t size){
    if(size > 64) {
        int n = size % 64;
        int i;
        for (i = 0; i < n; i++) {
            HAL_I2C_Mem_Read(&hi2c2,  EEPROM_ADDRESS, addr, EEPROM_MEMADD_SIZE_16BIT,  pBuffer + i * 64, 64, 100);
        }
        HAL_I2C_Mem_Read(&hi2c2,  EEPROM_ADDRESS, addr, EEPROM_MEMADD_SIZE_16BIT, pBuffer + n * 64, size - n*64, 100);
    }
    else{
        HAL_I2C_Mem_Read(&hi2c2,  EEPROM_ADDRESS, addr, EEPROM_MEMADD_SIZE_16BIT, pBuffer, size, 100);
    }

}