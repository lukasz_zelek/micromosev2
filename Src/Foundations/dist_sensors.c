//
// Created by mad on 09.01.19.
//

#include "Foundations/dist_sensors.h"
#include <math.h>
#include "Logic/Control/wall_detection.h"

extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc3;
extern TIM_HandleTypeDef htim6;
extern DMA_HandleTypeDef hdma_adc1;

extern uint8_t FLAG_WALL_FRONT;
extern uint8_t FLAG_WALL_LEFT;
extern uint8_t FLAG_WALL_RIGHT;

extern uint32_t DistSensorWynikA[6];
extern uint32_t DSW[6];
extern uint32_t DistSensorWynikB[6];
extern uint32_t *DistSensorWynik;

ADC_DIST_SENSOR sensor_select;
extern uint32_t *AmbientSensorWynik;
extern uint32_t AmbientSensorWynikA[6];
extern uint32_t AmbientSensorWynikB[6];
uint8_t parity = 0;


void DistSensorTimerStart(void) {

    HAL_TIM_Base_Start_IT(&htim6);
    sensor_select = LEFT_SIDE;
    FLAG_WALL_FRONT = 0;
    FLAG_WALL_LEFT = 0;
    FLAG_WALL_RIGHT = 0;

}

void DistSensorTimerStop(void) {

    HAL_TIM_Base_Stop_IT(&htim6);
    DistSensorTurnOffAll();
}

void DistSensorSetSensor(uint16_t ADC_DISTx) {
    ADC_ChannelConfTypeDef hadcx_initconfig;
    hadcx_initconfig.Rank = 1;
    hadcx_initconfig.SamplingTime = ADC_SAMPLETIME_7CYCLES_5;
    hadcx_initconfig.Channel = ADC_DISTx;
    HAL_ADC_ConfigChannel(&hadc1, &hadcx_initconfig);

}


uint16_t DistSensorMeassure(uint16_t ADC_DISTx) {
// TODO Wywalić referencje do DistSensorMeassure()
    //    DistSensorSetSensor(ADC_DISTx);
//    DistSensorTurnOn(ADC_DISTx);
//    HAL_ADC_Start(&hadc1);
//    while (HAL_ADC_PollForConversion(&hadc1, 50) != HAL_OK) {
//    }
//    DistSensorTurnOff(ADC_DISTx);
//
//    return  (uint16_t) HAL_ADC_GetValue(&hadc1);
    return (uint16_t) 0;
}

double DistSensorCalculateMilimeters(uint32_t x, ADC_DIST_SENSOR sensor) {
    //+/- 0.5 cm
    switch (sensor) {
        case LEFT_SIDE:
            return (double) (pow(DIST_SENSOR_MODEL_PARAM_A_LEFT_SIDE, log((double) x)) *
                             (double) DIST_SENSOR_MODEL_PARAM_B_LEFT_SIDE);
        case LEFT_FORWARD:
            return (double) (pow(DIST_SENSOR_MODEL_PARAM_A_LEFT_FORWARD, log((double) x)) *
                             (double) DIST_SENSOR_MODEL_PARAM_B_LEFT_FORWARD);
        case LEFT_DIAGONAL:
            return (double) (pow(DIST_SENSOR_MODEL_PARAM_A_LEFT_DIAGONAL, log((double) x)) *
                             (double) DIST_SENSOR_MODEL_PARAM_B_LEFT_DIAGONAL);
        case RIGHT_SIDE:
            return (double) (pow(DIST_SENSOR_MODEL_PARAM_A_RIGHT_SIDE, log((double) x)) *
                             (double) DIST_SENSOR_MODEL_PARAM_B_RIGHT_SIDE);
        case RIGHT_FORWARD:
            return (double) (pow(DIST_SENSOR_MODEL_PARAM_A_RIGHT_FORWARD, log((double) x)) *
                             (double) DIST_SENSOR_MODEL_PARAM_B_RIGHT_FORWARD);
        case RIGHT_DIAGONAL:
            return (double) (pow(DIST_SENSOR_MODEL_PARAM_A_RIGHT_DIAGONAL, log((double) x)) *
                             (double) DIST_SENSOR_MODEL_PARAM_B_RIGHT_DIAGONAL);
        default:
            return (double) 0;
    }

}


void DistSensorTurnOff(uint16_t ADC_DIST_x) {

    switch (ADC_DIST_x) {
        case ADC_DIST_RIGHT_FORWARD:
            HAL_GPIO_WritePin(DIST1_EN_GPIO_Port, DIST1_EN_Pin, GPIO_PIN_RESET);
            break;

        case ADC_DIST_LEFT_FORWARD:
            HAL_GPIO_WritePin(DIST2_EN_GPIO_Port, DIST2_EN_Pin, GPIO_PIN_RESET);
            break;

        case ADC_DIST_LEFT_DIAGONAL:
            HAL_GPIO_WritePin(DIST3_EN_GPIO_Port, DIST3_EN_Pin, GPIO_PIN_RESET);
            break;

        case ADC_DIST_RIGHT_DIAGONAL:
            HAL_GPIO_WritePin(DIST4_EN_GPIO_Port, DIST4_EN_Pin, GPIO_PIN_RESET);
            break;

        case ADC_DIST_RIGHT_SIDE:
            HAL_GPIO_WritePin(DIST5_EN_GPIO_Port, DIST5_EN_Pin, GPIO_PIN_RESET);
            break;

        case ADC_DIST_LEFT_SIDE:
            HAL_GPIO_WritePin(DIST6_EN_GPIO_Port, DIST6_EN_Pin, GPIO_PIN_RESET);
            break;

        default:
            break;
    }
}


void DistSensorTurnOffAll(void) {
    HAL_GPIO_WritePin(DIST1_EN_GPIO_Port, DIST1_EN_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(DIST2_EN_GPIO_Port, DIST2_EN_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(DIST3_EN_GPIO_Port, DIST3_EN_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(DIST4_EN_GPIO_Port, DIST4_EN_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(DIST5_EN_GPIO_Port, DIST5_EN_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(DIST6_EN_GPIO_Port, DIST6_EN_Pin, GPIO_PIN_RESET);
}


void DistSensorTurnOn(uint16_t ADC_DIST_x) {

    switch (ADC_DIST_x) {
        case ADC_DIST_RIGHT_FORWARD:
            HAL_GPIO_WritePin(DIST1_EN_GPIO_Port, DIST1_EN_Pin, GPIO_PIN_SET);
            break;

        case ADC_DIST_LEFT_FORWARD:
            HAL_GPIO_WritePin(DIST2_EN_GPIO_Port, DIST2_EN_Pin, GPIO_PIN_SET);
            break;

        case ADC_DIST_LEFT_DIAGONAL:
            HAL_GPIO_WritePin(DIST3_EN_GPIO_Port, DIST3_EN_Pin, GPIO_PIN_SET);
            break;

        case ADC_DIST_RIGHT_DIAGONAL:
            HAL_GPIO_WritePin(DIST4_EN_GPIO_Port, DIST4_EN_Pin, GPIO_PIN_SET);
            break;

        case ADC_DIST_RIGHT_SIDE:
            HAL_GPIO_WritePin(DIST5_EN_GPIO_Port, DIST5_EN_Pin, GPIO_PIN_SET);
            break;

        case ADC_DIST_LEFT_SIDE:
            HAL_GPIO_WritePin(DIST6_EN_GPIO_Port, DIST6_EN_Pin, GPIO_PIN_SET);
            break;

        default:
            break;
    }
}


void DistSensotTurnOnAll(void) {
    HAL_GPIO_WritePin(DIST1_EN_GPIO_Port, DIST1_EN_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(DIST2_EN_GPIO_Port, DIST2_EN_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(DIST3_EN_GPIO_Port, DIST3_EN_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(DIST4_EN_GPIO_Port, DIST4_EN_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(DIST5_EN_GPIO_Port, DIST5_EN_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(DIST6_EN_GPIO_Port, DIST6_EN_Pin, GPIO_PIN_SET);
}

void DistSensorTimerCallback(void) {
//static ADC_DIST_SENSOR sensor_select = LEFT_SIDE;
    //debug variables BEGIN
    //int xddddd = 6;
    //while (xddddd-- >= 0)
    //    DSW[xddddd]=DistSensorWynik[xddddd];
    //debug variables END
    parity = (parity + 1) % 6;//czestotliwosc probkowania na 1 czujniku: 80 Hz
    if (parity == 0) {
        DistSensorWynik[sensor_select] = HAL_ADC_GetValue(&hadc1) - AmbientSensorWynik[sensor_select];

        if (sensor_select == RIGHT_SIDE) {
            sensor_select = LEFT_SIDE;
            if (DistSensorWynik == (uint32_t *) DistSensorWynikA)
                DistSensorWynik = (uint32_t *) DistSensorWynikB;
            else
                DistSensorWynik = (uint32_t *) DistSensorWynikA;
            if (AmbientSensorWynik == (uint32_t *) AmbientSensorWynikA)
                AmbientSensorWynik = (uint32_t *) AmbientSensorWynikB;
            else
                AmbientSensorWynik = (uint32_t *) AmbientSensorWynikA;
        } else
            sensor_select++;

        switch (sensor_select) {

            case LEFT_SIDE:
                DistSensorTurnOff(ADC_DIST_RIGHT_SIDE);
                DistSensorSetSensor(ADC_DIST_LEFT_SIDE);
                break;

            case LEFT_DIAGONAL:
                DistSensorTurnOff(ADC_DIST_RIGHT_FORWARD);
                DistSensorSetSensor(ADC_DIST_LEFT_DIAGONAL);
                break;

            case LEFT_FORWARD:
                DistSensorTurnOff(ADC_DIST_RIGHT_DIAGONAL);
                DistSensorSetSensor(ADC_DIST_LEFT_FORWARD);
                break;

            case RIGHT_FORWARD:
                DistSensorTurnOff(ADC_DIST_LEFT_SIDE);
                DistSensorSetSensor(ADC_DIST_RIGHT_FORWARD);
                break;

            case RIGHT_DIAGONAL:
                DistSensorTurnOff(ADC_DIST_LEFT_DIAGONAL);
                DistSensorSetSensor(ADC_DIST_RIGHT_DIAGONAL);
                break;

            case RIGHT_SIDE:
                DistSensorTurnOff(ADC_DIST_LEFT_FORWARD);
                DistSensorSetSensor(ADC_DIST_RIGHT_SIDE);
                break;
        }
    }else if(parity == 2){
        //Detekcja sciany
        FLAG_WALL_FRONT = WallCheck_Front();
        FLAG_WALL_LEFT = WallCheck_Left();
        FLAG_WALL_RIGHT = WallCheck_Right();
    }else if (parity == 4) {
        //Pomiar tła
        AmbientSensorWynik[sensor_select] = HAL_ADC_GetValue(&hadc1);

        switch (sensor_select) {

            case LEFT_SIDE:
                DistSensorTurnOn(ADC_DIST_LEFT_SIDE);
                break;

            case LEFT_DIAGONAL:
                DistSensorTurnOn(ADC_DIST_LEFT_DIAGONAL);
                break;

            case LEFT_FORWARD:
                DistSensorTurnOn(ADC_DIST_LEFT_FORWARD);
                break;

            case RIGHT_FORWARD:
                DistSensorTurnOn(ADC_DIST_RIGHT_FORWARD);
                break;

            case RIGHT_DIAGONAL:
                DistSensorTurnOn(ADC_DIST_RIGHT_DIAGONAL);
                break;

            case RIGHT_SIDE:
                DistSensorTurnOn(ADC_DIST_RIGHT_SIDE);
                break;
        }

    }


    HAL_ADC_Start(&hadc1);
}